(function(){
    'use strict';

    angular.module("core", [
        "ngRoute", 
        "ngMessages", 
        "ui.bootstrap", 
        "chart.js",
        "ngMask",
        "froala"
    ]);

})();