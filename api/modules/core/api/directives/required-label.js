(function(){
    angular.module("core").directive("requiredLabel", requiredLabel);
    
    function requiredLabel(){
        return {
            restrict: 'A',
            require: "^^form",
            scope: {
                "requiredLabel": "@"
            },
            link: function ($scope, $element, $attributes, $form){
                $element.addClass("required-label");
                $scope.$watch(function(){
                    return $form.$submitted && $form[$scope.requiredLabel].$invalid
                }, function (submitted){
                    if(submitted && $form[$scope.requiredLabel].$invalid){
                        $element.addClass('invalid-label');
                    }else{
                        $element.removeClass('invalid-label');
                    }
                });
            }
        }
    }

})();