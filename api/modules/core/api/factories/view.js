(function(){
    'use strict';

    angular.module("core").factory("ViewFactory", ["$location", function($location){
        var $session = {};
        var $ctrl = {
            user: null,
            siteName: "POC - TJGO",
            siteLogo: "assets/img/logo.jpg",
            year: new Date().getFullYear(),
            isCollapsed: true,
            setSidebar: function(sidebar){
                $ctrl.sidebar = (sidebar) ? sidebar : $ctrl.sidebar;
            },
            setNavbar: function(navbar){
                $ctrl.navbar = (navbar) ? navbar : $ctrl.navbar;
            },
            setBreadcrumb: function(breadcrumb){
                $ctrl.breadcrumb = (breadcrumb) ? breadcrumb : $ctrl.breadcrumb;
            },
            setTitle: function(title){
                $ctrl.title = (title) ? title : $ctrl.title;
            },
            setUserMenu: function(userMenu){
                $ctrl.userMenu = (userMenu) ? userMenu : $ctrl.userMenu;
            },
            setView: function(title, breadcrumb, navbar, sidebar, userMenu){
                $ctrl.setTitle(title);
                $ctrl.setBreadcrumb(breadcrumb);
                $ctrl.setNavbar(navbar);
                $ctrl.setSidebar(sidebar);
                $ctrl.setUserMenu(userMenu);
            },
            init: null,
            getPath: function(indexPage){
                indexPage = (indexPage == undefined && isNaN(parseInt(indexPage))) ? 0 : indexPage;
                return $location.path().split('/')[indexPage+1]
            },
            setPath: function(url){
                $location.path(url.replace('#', ''));
            },
            setSession: function(sessionName, session, url){
                $session[sessionName] = session;
                if(url){
                    $location.path(url.replace('#', ''));
                }
            },
            getSessionList: function(sessionName){
                return (angular.isDefined($session[sessionName])) ? $session[sessionName] : [];
            },
            getSessionObject: function(sessionName, object){
                return (angular.isDefined($session[sessionName])) ? $session[sessionName] : (object ? object : {});
            },
            getSession: function(sessionName, value){
                return (angular.isDefined($session[sessionName])) ? $session[sessionName] : (value ? value : null);
            },
            clearSession: function(clearSession){
                if(angular.isArray(clearSession)){
                    if(clearSession.length && clearSession[0] == "all"){
                        $session = {};
                    }else{
                        for(var i=0; i<clearSession.length; i++){
                            delete $session[clearSession[i]];
                        }
                    }
                }
            }
        };
        
        return $ctrl;
    }]);

})();