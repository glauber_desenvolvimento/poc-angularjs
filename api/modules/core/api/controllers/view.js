(function(){
    'use strict';

    angular.module("core").controller("ViewController", ["$scope", "UserService", "VisualService", "ViewFactory", "$location", function($scope, UserService, VisualService, ViewFactory, $location){
        var $ctrl = this;
        $ctrl.view = ViewFactory;
        $ctrl.redirect = redirect;
        
        UserService.getLoggedUser().then(function(response){
            $ctrl.view.user = response;
        });

        $ctrl.view.init = function(){
            VisualService.getVisualControl().then(function(response){
                $ctrl.view.setView(response.title, response.breadcrumb, response.navbar, response.sidebar, response.userMenu);
            });
        }
        $ctrl.view.init();


        function redirect(url, clearSession){
            if(clearSession){
                ViewFactory.clearSession(clearSession);
            }
            ViewFactory.setPath(url);
        }

    }]);

})();