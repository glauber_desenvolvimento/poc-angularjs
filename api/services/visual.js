(function(){
    'use strict';

    angular.module("app").service("VisualService", ["VisualMock", function(VisualMock){
        var $ctrl = this;
        $ctrl.getVisualControl = getVisualControl;

        function getVisualControl(){
            return VisualMock.getVisualControl();
        }
    }]);

})();