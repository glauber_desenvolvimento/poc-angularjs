(function(){
    'use strict';

    angular.module("app").service("ParserService", [function(){
        
        var $ctrl = this;
        $ctrl.mappingList = mappingList;
        $ctrl.mappingObject = mappingObject;

        function mappingList(list, mapping){
            var formatedList = [];
            if(!mapping){
                return list;
            }
            for(var i = 0; i<list.length; i++){
                var item = list[i];
                var obj = {};
                for(var key in mapping){
                    var position = mapping[key];
                    obj[key] = item[position];
                }
                formatedList.push(obj);
            }
            return formatedList;
        }

        function mappingObject(object, mapping){
            var formatedObject = {};
            if(!mapping){
                return object;
            }else{
                for(var key in mapping){
                    var position = mapping[key];
                    formatedObject[key] = object[position];
                }
            }
            return formatedObject;
        }

    }]);

})();