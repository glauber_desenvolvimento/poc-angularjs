(function(){
    'use strict';
    
    angular.module("app").service("DaoService", ["ParserService", "$http", "$q", function(ParserService, $http, $q){
        var $ctrl = this;
        $ctrl.baseUrl = ""
        $ctrl.getResource = getResource;
        $ctrl.postResource = postResource;
        $ctrl.putResource = putResource;
        $ctrl.deleteResource = deleteResource;
        $ctrl.errorCallback = errorCallback;
        $ctrl.config = {};

        function errorCallback(response){
            console.log(response);
        }

        function getResource(resource, mapping){
            return $http.get($ctrl.baseUrl+"/"+resource).then(
                function(response){
                    return $q.resolve(ParserService.mappingList(response, mapping));
                },
                errorCallback
            );
        }

        function postResource(resource, data, mapping){
            return $http.post($ctrl.baseUrl+"/"+resource, data).then(
                function(response){
                    return $q.resolve(ParserService.mappingObject(response, mapping));
                }, errorCallback
            );
        }

        function putResource(resource, data, mapping){
            return $http.put($ctrl.baseUrl+"/"+resource, data).then(
                function(response){
                    return $q.resolve(ParserService.mappingObject(response, mapping));
                }, errorCallback
            );
        }

        function deleteResource(resource, data, mapping){
            return $http.post($ctrl.baseUrl+"/"+resource);
        }

    }]);

})();