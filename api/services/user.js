(function(){
    'use strict';

    angular.module("app").service("UserService", ["UserMock", "ParserService", function(UserMock, ParserService){
        var $ctrl = this;
        $ctrl.getLoggedUser = getLoggedUser;
        $ctrl.getList = getList;

        function getLoggedUser(){
            return UserMock.getLoggedUser();
        }
        function getList(){
            return UserMock.getList();
        }
    }]);

})();