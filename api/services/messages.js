(function(){
    'use strict';

    angular.module("app").service("MessagesService", ["MessagesMock", function(MessagesMock){
        var $ctrl = this;
        $ctrl.getMessages = getMessages;
        
        function getMessages(){
            return MessagesMock.getMessages();
        }

    }]);

})();