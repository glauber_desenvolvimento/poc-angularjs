(function(){
    'use strict';

    angular.module("app").factory("VisualMock", ["$q", function($q){
        
        var routeMapping = window.routeMapping;

        var $ctrl = {};
        $ctrl.getVisualControl = getVisualControl;

        var title = "Dashboard";
        var breadcrumb = [
            { title: "Home", icon: "fa fa-home", url: routeMapping[0].url },
            { title: "Dashboard" },
        ];
        var navbar = routeMapping.filter(function(route){
            return route.menu == "navbar";
        });
        var userMenu = routeMapping.filter(function(route){
            return route.menu == "user";
        });
        var sidebar = routeMapping.filter(function(route){
            return route.menu == "sidebar";
        });

        function getVisualControl(){
            return $q.resolve({
                title: title,
                breadcrumb: breadcrumb,
                navbar: navbar,
                userMenu: userMenu,
                sidebar: sidebar
            });
        }
        return $ctrl;
    }]);

})();