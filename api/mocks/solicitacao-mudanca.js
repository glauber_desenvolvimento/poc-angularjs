(function(){
    'use strict';

    angular.module("app").factory("SolicitacaoMudancaMock", ["$q", function($q){
        var $ctrl = {};
        $ctrl.getTipoSolicitacaoList = getTipoSolicitacaoList;
        $ctrl.getTipoSustentacaoList = getTipoSustentacaoList;
        $ctrl.getProjetosList = getProjetosList;
        $ctrl.getTipoServicoList = getTipoServicoList;
        $ctrl.getPerfilList = getPerfilList;
        $ctrl.getNiveisList = getNiveisList;
        $ctrl.getTabelasEnvolvidasList = getTabelasEnvolvidasList;
        
        var tiposSolicitacaoList = [
            { id: 1, descricao: "Sustentação" },
            { id: 2, descricao: "Metodologia" },
            { id: 3, descricao: "Teste" }
        ];

        var tipoSustentacaoList = [
            { id: 1, descricao: "Adaptativa" },
            { id: 2, descricao: "Evolutiva" },
            { id: 3, descricao: "Corretiva" }
        ];

        var projetosList = [
            { id: 1, descricao: "MDS – Metodologia de Desenvolvimento de Software" },
            { id: 2, descricao: "SS – Sustentação de Software" },
            { id: 3, descricao: "TS – Teste de Software" }
        ];

        var tipoServicoList = [
            { id: 1, descricao: "Serviços de Gerência" },
            { id: 2, descricao: "Serviços de Atendimento" }
        ];

        var perfilList = { 
            teste: [
                { id: 1, descricao: "Analistas de Teste" },
                { id: 2, descricao: "Gerente de Configuração e Mudança" }
            ],
            metodologia: [
                { id: 1, descricao: "Gerente de Projeto" },
                { id: 2, descricao: "Líder Técnico" },
                { id: 3, descricao: "Líder de Projeto" },
                { id: 4, descricao: "Designer" },
                { id: 5, descricao: "Arquiteto" },
                { id: 6, descricao: "Analista de processo" },
                { id: 7, descricao: "Analista de métrica" },
                { id: 8, descricao: "Analista de Requisito" },
                { id: 9, descricao: "GCM – Gerente de Configuração e Mudança" }
            ]
        }

        var niveisList = [
            { id: 1, descricao: "Médio" },
            { id: 2, descricao: "Baixo" },
            { id: 3, descricao: "Alto" }
        ];

        var tabelasEnvolvidasList = [
            { id: 1, descricao: "ProcessoParalisadoDt" },
            { id: 2, descricao: "ServentiaCargoDt" },
            { id: 3, descricao: "ServentiaDt" }
        ];
        
        function getTipoSolicitacaoList(){
            return $q.resolve(tiposSolicitacaoList);
        };

        function getTipoSustentacaoList(){
            return $q.resolve(tipoSustentacaoList);
        };

        function getProjetosList(){
            return $q.resolve(projetosList);
        };

        function getTipoServicoList(){
            return $q.resolve(tipoServicoList);
        };

        function getPerfilList(filtro){
            return $q.resolve(perfilList[filtro]);
        };

        function getNiveisList(){
            return $q.resolve(niveisList);
        }

        function getTabelasEnvolvidasList(){
            return $q.resolve(tabelasEnvolvidasList);
        }
        
        return $ctrl;
    }]);

})();