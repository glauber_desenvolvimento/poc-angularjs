(function(){
    'use strict';

    angular.module("app").factory("StakeholderMock", ["$q", function($q){
        var $ctrl = {};
        
        $ctrl.getGrupoList = getGrupoList;
        $ctrl.getLotacaoList = getLotacaoList;
        $ctrl.getPapelList = getPapelList;
        $ctrl.getEquipeList = getEquipeList;
        $ctrl.getResponsabilidadeList = getResponsabilidadeList;

        var grupoList = [
            {id: 1, descricao: "TJGO"},
            {id: 2, descricao: "CTIS"}
        ];

        var lotacaoList = [
            {id: 1, descricao: "DSI"},
            {id: 2, descricao: "TJGO"},
            {id: 3, descricao: "Brasília"}
        ];

        var equipeList = [
            {id: 1, descricao: "Equipe Técnica"},
            {id: 2, descricao: "Equipe Gerencial"}
        ]

        var responsabilidadeList = [
            {id: 1, descricao: "Aconpanhamento do Projeto"},
            {id: 2, descricao: "Auditoria Básica"},
            {id: 3, descricao: "Auditoria Completa"},
            {id: 4, descricao: "Implementação Java"},
            {id: 5, descricao: "Realização de Testes"},
            {id: 6, descricao: "Realização de Entrega"},
            {id: 7, descricao: "Elaboração de Metodologia"},
            {id: 8, descricao: "Realização de Qualidade"},
            {id: 9, descricao: "Validação de Entrega"}
        ]

        var papelList = [
            {id: 1, descricao: "Fiscal Técnico", tipo: "Equipe Técnica"},
            {id: 2, descricao: "Analista de Testes", tipo: "Equipe Técnica"},
            {id: 3, descricao: "Gerente de Departamento", tipo: "Equipe Gerencial"},
            {id: 4, descricao: "Gerente de Requisitos", tipo: "Equipe Gerencial"},
            {id: 5, descricao: "Gestor de Contrato", tipo: "Equipe Gerencial"},
            {id: 6, descricao: "Gerente de Projetos", tipo: "Equipe Gerencial"},
            {id: 7, descricao: "Gerente de Processos", tipo: "Equipe Gerencial"}
        ];

        function getGrupoList(){
            return $q.resolve(grupoList);
        }

        function getLotacaoList(){
            return $q.resolve(lotacaoList);
        }

        function getPapelList(){
            return $q.resolve(papelList);
        }

        function getEquipeList(){
            return $q.resolve(equipeList);
        }

        function getResponsabilidadeList(){
            return $q.resolve(responsabilidadeList);
        }

        return $ctrl;
    }]);
    
})();