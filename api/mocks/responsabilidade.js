(function(){
    'use strict';

    angular.module("app").factory("ResponsabilidadeMock", ["$q", function($q){
        var $ctrl = {};
        $ctrl.getMatrizResponsabilidades = getMatrizResponsabilidades;
        
        var matrizResponsabilidades = [
            {
                stakeholder: null,
                recurso: {
                    nome: "Daniele da Silva",
                    papel: "Analista de Processo",
                    horarioTrabalho: "Das 08:00 às 18:00",
                    telefone: "61 32129539",
                    horasTrabalhadas: 148,
                    agenda: { manha: { quarta: true, quinta: true, segunda: true, sexta: true, terca: true } },
                    alocacao: "Part-time",
                    equipe: "Equipe Técnica",
                    projetos: ["PLANEJAMENTO REFERENTE AO PLANO DE GERENCIA DE CONFIGURAÇÃO E MUDANÇA","METODOLOGIA DE DESENVOLVIMENTO DE SOFTWARE CONTAGEM DETALHADA SERVICOS GERENCIA DE PROJETOS","METODOLOGIA DE DESENVOLVIMENTO DE SOFTWARE ARTEFATOS DE METRICA E INICIAÇÃO"],
                    responsabilidades: ["Implementação Java","Realização de Testes","Realização de Entrega"] 
                }
            },
            {
                stakeholder: {
                    nome: "Ana Claudia Loureiro",
                    grupo: "TJGO",
                    lotacao: "DSI",
                    papel: "Gestor de Contrato",
                    telefone: "(99) 99999-9999",
                    email: "joaoamoedo@gmail.com",
                    equipe: "Equipe Gerencial",
                    projetos: ["METODOLOGIA DE DESENVOLVIMENTO DE SOFTWARE CONTAGEM DETALHADA SERVICOS GERENCIA DE PROJETOS", "METODOLOGIA DE DESENVOLVIMENTO DE SOFTWARE ARTEFATOS DE METRICA E INICIAÇÃO", "METODOLOGIA E DESENVOLVIMENTO DE SOFTWARE"],
                    responsabilidades: [ "Aconpanhamento do Projeto", "Auditoria Básica", "Auditoria Completa" ]
                },
                recurso: null
            }
        ];

        function getMatrizResponsabilidades(){
            return $q.resolve(matrizResponsabilidades);
        }

        return $ctrl;
    }]);
    
})();