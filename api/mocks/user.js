(function(){
    'use strict';

    angular.module("app").factory("UserMock", ["$q", function($q){
        var $ctrl = {};
        $ctrl.getLoggedUser = getLoggedUser;
        $ctrl.getList = getList;

        var user = {
            login: "ana_claudia",
            name: "Ana Claudia",
            email: "cbloureiro@tjgo.jus.br",
            telefone: "62 3216-2422",
            profile: "assets/img/profile.png",
            rule: ["Administrador"]
        };
        
        var userList = [ user, 
            {
                login: "willian_cesar",
                name: "Willian Cesar",
                email: "william.guiraldelli@ctis.com.br",
                telefone: "(61) 3212-9500",
                profile: "assets/img/profile.png",
                rule: ["Gerente"]
            }
        ];

        function getLoggedUser(){
            return $q.resolve(user);
        }
        function getList(){
            return $q.resolve(userList);
        }

        return $ctrl;
    }]);

})();