(function(){
    'use strict';

    angular.module("app").factory("OSMock", ["$q", function($q){
        var $ctrl = {};
        $ctrl.getTipoOSList = getTipoOSList;
        $ctrl.getPerfilList = getPerfilList;
        $ctrl.getAtoresList = getAtoresList;
        $ctrl.getRecursosList = getRecursosList;
        $ctrl.getTipoArquivoList = getTipoArquivoList;
        
        var tipoOSList = [
            { id: 1, descricao: "Novos Projetos" },
            { id: 2, descricao: "Metodologia" },
            { id: 3, descricao: "Teste" },
            { id: 4, descricao: "Metodologia, Sustentação Evolutiva e Teste" }
        ];
        
        var perfilList = [
            { id: 1, descricao: "Analista de Processos" },
            { id: 2, descricao: "Analista de Requisitos" },
            { id: 3, descricao: "Gerente de Projetos" },
            { id: 4, descricao: "Analista Java" },
            { id: 5, descricao: "Designer" },
            { id: 6, descricao: "Analista de Teste" }
        ];

        var atoresList = {
            novosProjetos: [
                { id: 1, descricao: "Analista de Sistemas" },
                { id: 2, descricao: "Analista de Requisitos" },
                { id: 3, descricao: "Analista de Testes" },
                { id: 4, descricao: "Analista de Processos" },
                { id: 5, descricao: "GCM – Gerente de Configuração e Mudança" },
                { id: 6, descricao: "Analista Java" },
                { id: 7, descricao: "Gerente de Projeto" }
            ], teste: [
                { id: 1, descricao: "Analista de Teste" },
                { id: 2, descricao: "GCM – Gerente de Configuração e Mudança" },
                { id: 3, descricao: "Gerente de Projetos" }
            ]
        };

        var recursosList = [
            { id: 1, descricao: "Gerente de Projeto" },
            { id: 2, descricao: "Líder Técnico" },    
            { id: 3, descricao: "Líder de Projeto" },
            { id: 4, descricao: "Designer" },
            { id: 5, descricao: "Arquiteto" },
            { id: 6, descricao: "Analista de processo" },
            { id: 7, descricao: "Analista de métrica" },
            { id: 8, descricao: "Analista de Requisito" },
            { id: 9, descricao: "GCM – Gerente de Configuração e Mudança" }
        ];
        
        var tipoArquivoList = [
            { id: 1, descricao: "Declaração de Escopo" },
            { id: 2, descricao: "Metodologia de Gerência de Configuração e Mudança" },
            { id: 3, descricao: "Metodologia de Desenvolvimento de Software" },
            { id: 4, descricao: "Metodologia de Gestão de Projeto" }
        ];

        function getTipoOSList(){
            return $q.resolve(tipoOSList);
        };

        function getPerfilList(){
            return $q.resolve(perfilList);
        }

        function getAtoresList(filtro){
            return $q.resolve(atoresList[filtro]);
        }

        function getRecursosList(){
            return $q.resolve(recursosList);
        }

        function getTipoArquivoList(){
            return $q.resolve(tipoArquivoList);
        }
        
        return $ctrl;
    }]);
    
})();