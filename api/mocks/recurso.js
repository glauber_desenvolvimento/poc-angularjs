(function(){
    'use strict';

    angular.module("app").factory("RecursoMock", ["$q", function($q){
        var $ctrl = {};
        
        $ctrl.getPapelList = getPapelList;
        $ctrl.getAlocacaoList = getAlocacaoList;
        $ctrl.getEquipeList = getEquipeList;
        $ctrl.getResponsabilidadeList = getResponsabilidadeList;

        var papelList = [
            {id: 1, descricao: "Analista de Processo", tipo: "Equipe Técnica"},
            {id: 2, descricao: "Designer / Analista Java", tipo: "Equipe Técnica"},
            {id: 3, descricao: "Arquiteto", tipo: "Equipe Técnica"},
            {id: 4, descricao: "Arquiteto Líder", tipo: "Equipe Técnica"},
            {id: 5, descricao: "Líder Técnico", tipo: "Equipe Técnica"},
            {id: 6, descricao: "Fiscal Técnico", tipo: "Equipe Técnica"},
            {id: 7, descricao: "Analista de Testes", tipo: "Equipe Técnica"},
            {id: 8, descricao: "Gerente de Departamento", tipo: "Equipe Técnica"},
            {id: 9, descricao: "Analista de Métricas", tipo: "Equipe Técnica"},
            {id: 10, descricao: "Gerente de Requisitos", tipo: "Equipe Gerencial"},
            {id: 11, descricao: "Gestor de Contrato", tipo: "Equipe Gerencial"},
            {id: 12, descricao: "Gerente de Projetos", tipo: "Equipe Gerencial"},
            {id: 13, descricao: "Gerente de Processos", tipo: "Equipe Gerencial"},
            {id: 14, descricao: "Gerente de Configuração e Mundança", tipo: "Equipe Gerencial"}
        ]

        var responsabilidadeList = [
            {id: 1, descricao: "Aconpanhamento do Projeto"},
            {id: 2, descricao: "Auditoria Básica"},
            {id: 3, descricao: "Auditoria Completa"},
            {id: 4, descricao: "Implementação Java"},
            {id: 5, descricao: "Realização de Testes"},
            {id: 6, descricao: "Realização de Entrega"},
            {id: 7, descricao: "Elaboração de Metodologia"},
            {id: 8, descricao: "Realização de Qualidade"},
            {id: 9, descricao: "Validação de Entrega"}
        ]

        var alocacaoList = [
            { descricao: "Part-time", id: 1 },
            { descricao: "Full-time", id: 2 }
        ]

        var equipeList = [
            {id: 1, descricao: "Equipe Técnica"},
            {id: 2, descricao: "Equipe Gerencial"}
        ]
        
        function getPapelList(){
            return $q.resolve(papelList);
        }

        function getAlocacaoList(){
            return $q.resolve(alocacaoList);
        }

        function getEquipeList(){
            return $q.resolve(equipeList);
        }

        function getResponsabilidadeList(){
            return $q.resolve(responsabilidadeList);
        }

        return $ctrl;
    }]);
    
})();