(function(){
    'use strict';

    angular.module("app").factory("MessagesMock", ["$q", function($q){
        var $ctrl = {};
        $ctrl.getMessages = getMessages;

        var messages = {
            required: "Este campo é de preenchimento obrigatório.",
            email: "Este campo deve ser um e-mail válido.",
            number: "Este campo deve ser um número.",
            alertGrid: "Por favor preencher todos os campos corretamente"
        }

        function getMessages(){
            return $q.resolve(messages);
        }

        return $ctrl;
    }]);

})();