(function(){
    'use strict';
    
    angular.module("app").factory("TapMock", ["$q", function($q){
        var $ctrl = {};
        $ctrl.getMesReferenciaList = getMesReferenciaList;
        $ctrl.getClassificacaoList = getClassificacaoList;
        $ctrl.getUnidadeMedidaList = getUnidadeMedidaList;
        $ctrl.getProjetosList = getProjetosList;
        $ctrl.getPapeisList = getPapeisList;
        $ctrl.getAvaliacaoList = getAvaliacaoList;
        $ctrl.getOSList = getOSList;
        
        var projetosList = [
            {
                dataTAP: new Date("2018-01-01T00:00:00.000-03:00"), 
                mesReferencia: {descricao: "Janeiro", id: 1}, 
                classificacao: {descricao: "Cliente", id: 2}, 
                descricaoProjeto: "PLANEJAMENTO REFERENTE AO PLANO DE GERENCIA DE CONFIGURAÇÃO E MUDANÇA",
                dataPrevisaoInicio: new Date("2018-01-01T00:00:00.000-03:00"),
                dataPrevisaoEntrega: new Date("2018-02-01T00:00:00.000-03:00"),
                orcamento: {
                    tipo: { id: 2, descricao: "Metodologia" },
                    unidadeMedida: {id: 1, descricao: "PF"},
                    quantidade: 20,
                    valorUnitario: 669,
                },
                cronograma: {
                    dataPrevistaInicial: new Date("2018-01-01T00:00:00.000-03:00"),
                    dataPrevistaEntrega: new Date("2018-02-01T00:00:00.000-03:00"),
                    os: { id: 1, descricao: "10001" }
                }
            },
            {
                dataTAP: new Date("2018-02-01T00:00:00.000-03:00"), 
                mesReferencia: {descricao: "Fevereiro", id: 2}, 
                classificacao: {descricao: "Corporativo", id: 1}, 
                descricaoProjeto: "METODOLOGIA DE DESENVOLVIMENTO DE SOFTWARE CONTAGEM DETALHADA SERVICOS GERENCIA DE PROJETOS",
                dataPrevisaoInicio: new Date("2018-02-01T00:00:00.000-03:00"),
                dataPrevisaoEntrega: new Date("2018-03-01T00:00:00.000-03:00"),
                orcamento: {
                    tipo: { id: 2, descricao: "Metodologia" },
                    unidadeMedida: {id: 1, descricao: "PF"},
                    quantidade: 17.5,
                    valorUnitario: 669,
                },
                cronograma: {
                    dataPrevistaInicial: new Date("2018-02-01T00:00:00.000-03:00"),
                    dataPrevistaEntrega: new Date("2018-03-01T00:00:00.000-03:00"),
                    os: { id: 2, descricao: "10002" }
                }
            },
            {
                dataTAP: new Date("2018-03-01T00:00:00.000-03:00"), 
                mesReferencia: {descricao: "Março", id: 3}, 
                classificacao: {descricao: "Corporativo", id: 1}, 
                descricaoProjeto: "METODOLOGIA DE DESENVOLVIMENTO DE SOFTWARE ARTEFATOS DE METRICA E INICIAÇÃO",
                dataPrevisaoInicio: new Date("2018-03-01T00:00:00.000-03:00"),
                dataPrevisaoEntrega: new Date("2018-04-01T00:00:00.000-03:00"),
                orcamento: {
                    tipo: { id: 2, descricao: "Metodologia" },
                    unidadeMedida: {id: 1, descricao: "PF"},
                    quantidade: 16.5,
                    valorUnitario: 669,
                },
                cronograma: {
                    dataPrevistaInicial: new Date("2018-03-01T00:00:00.000-03:00"),
                    dataPrevistaEntrega: new Date("2018-04-01T00:00:00.000-03:00"),
                    os: { id: 3, descricao: "10003" }
                }
            },
            {
                dataTAP: new Date("2018-04-01T00:00:00.000-03:00"), 
                mesReferencia: {descricao: "Abril", id: 4}, 
                classificacao: {descricao: "Cliente", id: 2}, 
                descricaoProjeto: "METODOLOGIA E DESENVOLVIMENTO DE SOFTWARE",
                dataPrevisaoInicio: new Date("2018-04-01T00:00:00.000-03:00"),
                dataPrevisaoEntrega: new Date("2018-05-01T00:00:00.000-03:00"),
                orcamento: {
                    tipo: { id: 2, descricao: "Metodologia" },
                    unidadeMedida: {id: 1, descricao: "PF"},
                    quantidade: 21.25,
                    valorUnitario: 669,
                },
                cronograma: {
                    dataPrevistaInicial: new Date("2018-04-01T00:00:00.000-03:00"),
                    dataPrevistaEntrega: new Date("2018-05-01T00:00:00.000-03:00"),
                    os: { id: 4, descricao: "10004" }
                }
            },
            {
                dataTAP: new Date("2018-05-01T00:00:00.000-03:00"), 
                mesReferencia: {descricao: "Maio", id: 5}, 
                classificacao: {descricao: "Corporativo", id: 1}, 
                descricaoProjeto: "DOCUMENTO DESIGNER PADRÃO MODELO DE ACESSIBILIDADE EM GOVERNO ELETRÔNICO",
                dataPrevisaoInicio: new Date("2018-05-01T00:00:00.000-03:00"),
                dataPrevisaoEntrega: new Date("2018-06-01T00:00:00.000-03:00"),
                orcamento: {
                    tipo: { id: 2, descricao: "Metodologia" },
                    unidadeMedida: {id: 1, descricao: "PF"},
                    quantidade: 22.50,
                    valorUnitario: 669,
                },
                cronograma: {
                    dataPrevistaInicial: new Date("2018-05-01T00:00:00.000-03:00"),
                    dataPrevistaEntrega: new Date("2018-06-01T00:00:00.000-03:00"),
                    os: { id: 5, descricao: "10005" }
                }
            },
            {
                dataTAP: new Date("2018-06-01T00:00:00.000-03:00"), 
                mesReferencia: {descricao: "Junho", id: 6}, 
                classificacao: {descricao: "Corporativo", id: 1}, 
                descricaoProjeto: "WORKFLOW DE FERRAMENTA DE GESTAO DE PROJETOS",
                dataPrevisaoInicio: new Date("2018-06-01T00:00:00.000-03:00"),
                dataPrevisaoEntrega: new Date("2018-07-01T00:00:00.000-03:00"),
                orcamento: {
                    tipo: { id: 2, descricao: "Metodologia" },
                    unidadeMedida: {id: 1, descricao: "PF"},
                    quantidade: 15.00,
                    valorUnitario: 669,
                }
            },
            {
                dataTAP: new Date("2018-07-01T00:00:00.000-03:00"), 
                mesReferencia: {descricao: "Julho", id: 7}, 
                classificacao: {descricao: "Corporativo", id: 1}, 
                descricaoProjeto: "DOCUMENTOS GERENCIA DE CONFIGURAÇÃO E MUDANÇA E PROPOSTA FERRAMENTA",
                dataPrevisaoInicio: new Date("2018-07-01T00:00:00.000-03:00"),
                dataPrevisaoEntrega: new Date("2018-08-01T00:00:00.000-03:00"),
                orcamento: {
                    tipo: { id: 2, descricao: "Metodologia" },
                    unidadeMedida: {id: 1, descricao: "PF"},
                    quantidade: 16.25,
                    valorUnitario: 669,
                }
            },
            {
                dataTAP: new Date("2018-08-01T00:00:00.000-03:00"), 
                mesReferencia: {descricao: "Agosto", id: 8}, 
                classificacao: {descricao: "Cliente", id: 2}, 
                descricaoProjeto: "PROPOSTA PADRÃO DE TESTE E QUALIDADE",
                dataPrevisaoInicio: new Date("2018-08-01T00:00:00.000-03:00"),
                dataPrevisaoEntrega: new Date("2018-09-01T00:00:00.000-03:00"),
                orcamento: {
                    tipo: { id: 2, descricao: "Metodologia" },
                    unidadeMedida: {id: 1, descricao: "PF"},
                    quantidade: 21.25,
                    valorUnitario: 669,
                }
            }
        ];

        var mesReferenciaList = [
            { id: 1, descricao: "Janeiro" },
            { id: 2, descricao: "Fevereiro" },
            { id: 3, descricao: "Março" },
            { id: 4, descricao: "Abril" },
            { id: 5, descricao: "Maio" },
            { id: 6, descricao: "Junho" },
            { id: 7, descricao: "Julho" },
            { id: 8, descricao: "Agosto" },
            { id: 9, descricao: "Setembro" },
            { id: 10, descricao: "Outubro" },
            { id: 11, descricao: "Novembro" },
            { id: 12, descricao: "Dezembro" }
        ];

        var classificacaoList = [
            { id: 1, descricao: "Corporativo" },
            { id: 2, descricao: "Cliente" }
        ];

        var unidadeMedidaList = [
            { id: 1, descricao: "PF" },
            { id: 2, descricao: "Horas" }
        ];

        var papeisList = [
            { id: 1, descricao: "Analista de Processo" },
            { id: 2, descricao: "Arquiteto" },
            { id: 3, descricao: "Analista de Métricas" },
            { id: 4, descricao: "Designer / Analista Java" },
            { id: 5, descricao: "Analista de Teste" },
            { id: 6, descricao: "Gerente de Configuração e Mundança" },
            { id: 7, descricao: "Gerente de Processos" },
            { id: 8, descricao: "Líder Técnico" },
            { id: 9, descricao: "Arquiteto Líder" },
            { id: 10, descricao: "Gerente de Projeto" }
        ];

        var avaliacaoList = [
            {id: 1, descricao: 'Insulficiente'},
            {id: 2, descricao: 'Regular'},
            {id: 3, descricao: 'Bom'},
            {id: 4, descricao: 'Muito Bom'},
            {id: 5, descricao: 'Excelente'}
        ];

        var OSList = [
            {id: 1, descricao: "10001"},
            {id: 2, descricao: "10002"},
            {id: 3, descricao: "10003"},
            {id: 4, descricao: "10004"},
            {id: 5, descricao: "10005"}
        ];

        function getMesReferenciaList(){
            return $q.resolve(mesReferenciaList);
        }

        function getClassificacaoList(){
            return $q.resolve(classificacaoList);
        }

        function getUnidadeMedidaList(){
            return $q.resolve(unidadeMedidaList);
        }

        function getProjetosList(){
            return $q.resolve(projetosList);
        }

        function getPapeisList(){
            return $q.resolve(papeisList);
        }

        function getAvaliacaoList(){
            return $q.resolve(avaliacaoList);
        }

        function getOSList(){
            return $q.resolve(OSList);
        }

        return $ctrl;
    }]);
})();