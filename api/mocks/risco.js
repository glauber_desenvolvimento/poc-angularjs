(function(){
    'use strict';

    angular.module("app").factory("RiscoMock", ["$q", function($q){
        var $ctrl = {};
        $ctrl.getCategoriaList = getCategoriaList;
        $ctrl.getSituacaoList = getSituacaoList;
        
        var categoriaList = [
            {id: 1, descricao: "Baixo"},
            {id: 2, descricao: "Médio"},
            {id: 3, descricao: "Alto"}
        ];

        var situacaoList = [
            {id: 1, descricao: "Ocorreu"},
            {id: 2, descricao: "Não ocorreu"},
            {id: 3, descricao: "Risco eliminado"}
        ];

        function getCategoriaList(){
            return $q.resolve(categoriaList);
        }
            
        function getSituacaoList(){
            return $q.resolve(situacaoList);
        }

        return $ctrl;
    }]);
    
})();