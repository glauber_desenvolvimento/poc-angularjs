(function(){
    'use strict';

    angular.module("app").factory("PlanoComunicacaoMock", ["$q", function($q){
        var $ctrl = {};
        $ctrl.getResponsavelList = getResponsavelList;
        $ctrl.getMetodologiaList = getMetodologiaList;
        $ctrl.getPublicoEnvolvidoList = getPublicoEnvolvidoList;
        $ctrl.getFrequenciaList = getFrequenciaList;
        
        var publicoEnvolvidoList = [
            {id: 1, descricao: "Todos os Stakeholders"},
            {id: 2, descricao: "Equipe Técnica CTIS"}
        ];

        var responsavelList = [
            {id: 1, descricao: "Equipe CTIS"}
        ];

        var metodologiaList = [
            {id: 1, descricao: "Reunião Convencional"}
        ];

        var frequenciaList = [
            {id: 1, descricao: "Unica"}
        ];

        function getPublicoEnvolvidoList(){
            return $q.resolve(publicoEnvolvidoList);
        }
            
        function getResponsavelList(){
            return $q.resolve(responsavelList);
        }

        function getMetodologiaList(){
            return $q.resolve(metodologiaList);
        }

        function getFrequenciaList(){
            return $q.resolve(frequenciaList);
        }

        return $ctrl;
    }]);
    
})();