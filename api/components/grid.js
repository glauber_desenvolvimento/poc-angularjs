(function(){
    angular.module("app").component("grid", {
        controller: GridController,
        templateUrl: 'api/templates/components/grid.html',
        bindings: {
            ngModel: "=",
            fields: "=",
            alert: "@",
            title: "@"
        }
    });
    
    function GridController($scope, $filter){
        
        var $ctrl = this;
        $ctrl.save = save;
        $ctrl.edit = edit;
        $ctrl.remove = remove;
        $ctrl.$onInit = $onInit;
        $ctrl.getLabelByField = getLabelByField;

        function $onInit(){
            $ctrl.ngModel = (angular.isArray($ctrl.ngModel)) ? $ctrl.ngModel : [];
            $ctrl.model = {};
            $ctrl.index = -1;
            $ctrl.showAlert = false;
        }
        
        function save(){
            if(verifyRequired()){
                $ctrl.showAlert = true;
                window.setTimeout(function(){
                    $ctrl.showAlert = false;
                }, 2000);
                return;
            }
            if($ctrl.index < 0){
                $ctrl.ngModel.push(angular.copy($ctrl.model));
                $ctrl.model = {};
            }else{
                $ctrl.ngModel[$ctrl.index] = $ctrl.model;
                $ctrl.model = {};
                $ctrl.index = -1;
            }
        }

        function edit(item, index){
            $ctrl.index = index;
            $ctrl.model = angular.copy(item);
        }

        function remove(index){
            $ctrl.ngModel.splice(index, 1);
            $ctrl.index = -1;
        }

        function verifyRequired(){
            for(var i=0; i<$ctrl.fields.length; i++){
                var id = $ctrl.fields[i].id;
                if($ctrl.fields[i].type == "period"){
                    if(!$ctrl.model[id+"Begin"] && $ctrl.fields[i].required == true){
                        return true;
                    }
                    if(!$ctrl.model[id+"Final"] && $ctrl.fields[i].required == true){
                        return true;
                    }
                }else{
                    if(!$ctrl.model[id] && $ctrl.fields[i].required == true){
                        return true;
                    }
                }
            }
            return false;
        }

        function getLabelByField(field, item){
            if(field.type == "date"){
                return $filter('date')(item[field.id], 'dd/MM/yyyy');
            }else if(field.type == "period"){
                return $filter('date')(item[field.id+'Begin'], 'dd/MM/yyyy') + " a " + $filter('date')(item[field.id+'Final'], 'dd/MM/yyyy');
            }else if(field.type == "list"){
                return item[field.id][(field.labelList) ? field.labelList : 'descricao'];
            }else{
                return item[field.id];
            }
        }
    }
})();