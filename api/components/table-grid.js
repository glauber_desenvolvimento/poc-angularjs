(function(){
    angular.module("app").component("tableGrid", {
        controller: TableGrid,
        templateUrl: 'api/templates/components/table-grid.html',
        bindings: {
            ngModel: "=",
            fields: "<",
            actions: "<",
            footer: "@?",
            classFotter: "@?",
            noFoundMessage: "@?"
        }
    });
    
    function TableGrid($scope, $filter){
        var $ctrl = this;
        $ctrl.$onInit = $onInit;
        $ctrl.getLabelByField = getLabelByField;
        
        function $onInit(){
            
        }

        function getLabelByField(field, item){

            var arrId = field.id.split('.');
            var id = arrId[0];
            if(arrId.length > 0){
                for(var i = 0; i<arrId.length-1; i++){
                    item = (item[id]) ? item[id] : {};
                    id = arrId[i+1];
                }
            }
            if(!item[id]){
                return '';
            }else if(field.type == "date"){
                return $filter('date')(item[id], 'dd/MM/yyyy');
            }else if(field.type == "period"){
                return $filter('date')(item[id+'Begin'], 'dd/MM/yyyy') + " a " + $filter('date')(item[id+'Final'], 'dd/MM/yyyy');
            }else if(field.type == "list"){
                return item[id][(field.labelList) ? field.labelList : 'descricao'];
            }else if(field.type == 'currency'){
                return $filter('currency')(item[id])
            }else if(field.type == 'number'){
                return $filter('number')(item[id], 2)
            }else{
                return item[id];
            }
        }

    }

})();