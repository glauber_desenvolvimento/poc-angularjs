(function(){
    let prefixUrl = (window.location.origin != "http://localhost:3000") ? "#" : "";
    window.routeMapping = [
        {
            url: prefixUrl+"/", 
            title: "Home",
            icon: null, 
            menu: null, 
            clearSession: [], 
            routing: { controller: "HomeController", templateUrl: "home/" }
        },
        {
            url: prefixUrl+"/projetos", 
            title: "Projetos",
            icon: "fa fa-list",
            menu: "navbar",
            clearSession: [], 
            routing: { controller: "TapController", templateUrl: "tap/" }
        },
        {
            url: prefixUrl+"/orcamentos", 
            title: "Orçamentos",
            icon: "fa fa-list",
            menu: "navbar",
            clearSession: [], 
            routing: { controller: "OrcamentoController", templateUrl: "orcamento/" }
        },
        {
            url: prefixUrl+"/cronograma-inicial", 
            title: "Cronogramas",
            icon: "fa fa-list",
            menu: "navbar",
            clearSession: [], 
            routing: { controller: "CronogramaInicialController", templateUrl: "cronograma-inicial/" }
        },
        {
            url: prefixUrl+"/riscos", 
            title: "Riscos",
            icon: "fa fa-list",
            menu: "navbar",
            clearSession: [], 
            routing: { controller: "RiscoController", templateUrl: "risco/" }
        },
        {
            url: prefixUrl+"/controle-versao", 
            title: "Controle de Versão",
            icon: "fa fa-plus",
            // menu: "sidebar",
            clearSession: [], 
            routing: { controller: "ControleVersaoController", templateUrl: "controle-versao/criar/" }
        },
        
        {
            url: prefixUrl+"/termo-abertura-projeto", 
            title: "TAP - Termo de Abertura de Projeto",
            icon: "fa fa-plus",
            menu: "sidebar",
            clearSession: ['projetoSelected', 'index'], 
            routing: { controller: "TapController", templateUrl: "tap/criar/" }
        },
        {
            url: prefixUrl+"/termo-encerramento-projeto", 
            title: "TAP - Termo de Encerramento de Projeto",
            icon: "fa fa-plus",
            // menu: "sidebar",
            clearSession: [], 
            routing: { controller: "TepController", templateUrl: "tep/" }
        },
        {
            url: prefixUrl+"/orcamento-projeto", 
            title: "Orçamento Projeto",
            icon: "fa fa-plus",
            // menu: "sidebar",
            clearSession: [], 
            routing: { controller: "OrcamentoController", templateUrl: "orcamento/criar/" }
        },
        {
            url: prefixUrl+"/plano-comunicacao", 
            title: "Plano de Comunicação",
            icon: "fa fa-plus",
            // menu: "sidebar",
            clearSession: [], 
            routing: { controller: "PlanoComunicacaoController", templateUrl: "plano-comunicacao/criar/" }
        },
        {
            url: prefixUrl+"/controles-versoes", 
            title: "Controles de Versões",
            icon: "fa fa-list",
            menu: "navbar",
            clearSession: [], 
            routing: { controller: "ControleVersaoController", templateUrl: "controle-versao/" }
        },
        {
            url: prefixUrl+"/planos-comunicacoes", 
            title: "Comunicações",
            icon: "fa fa-list",
            menu: "navbar",
            clearSession: [], 
            routing: { controller: "PlanoComunicacaoController", templateUrl: "plano-comunicacao/" }
        },
        {
            url: prefixUrl+"/risco", 
            title: "Risco",
            icon: "fa fa-plus",
            // menu: "sidebar",
            clearSession: [], 
            routing: { controller: "RiscoController", templateUrl: "risco/criar/" }
        },
        {
            url: prefixUrl+"/recurso", 
            title: "Recurso",
            icon: "fa fa-plus",
            menu: "sidebar",
            clearSession: [], 
            routing: { controller: "RecursoController", templateUrl: "recurso/criar/" }
        },
        {
            url: prefixUrl+"/stakeholder", 
            title: "Stakeholder",
            icon: "fa fa-plus",
            menu: "sidebar",
            clearSession: [], 
            routing: { controller: "StakeholderController", templateUrl: "stakeholder/criar/" }
        },
        {
            url: prefixUrl+"/matriz-responsabilidades", 
            title: "Responsabilidades",
            icon: "fa fa-list",
            menu: "navbar",
            clearSession: [], 
            routing: { controller: "ResponsabilidadeController", templateUrl: "matriz-responsabilidades/" }
        },
        {
            url: prefixUrl+"/solicitacao-mudanca", 
            title: "Solicitação de Mudança",
            icon: "fa fa-plus",
            menu: "sidebar",
            clearSession: [], 
            routing: { controller: "SolicitacaoMudancaController", templateUrl: "sm/criar/" }
        },
        {
            url: prefixUrl+"/abertura-os", 
            title: "Abertura OS",
            icon: "fa fa-plus",
            menu: "sidebar",
            clearSession: [], 
            routing: { controller: "AberturaOSController", templateUrl: "os/criar/" }
        },
        {
            url: prefixUrl+"/recebimento-definitivo", 
            title: "Recebimento Definitivo",
            icon: "fa fa-plus",
            menu: "sidebar",
            clearSession: [], 
            routing: { controller: "AberturaOSController", templateUrl: "os/criar/" }
        },
        {
            url: prefixUrl+"/gerenciar-usuarios", 
            title: "Gerenciar Usuários",
            icon: "fa fa-list",
            menu: "user",
            clearSession: [], 
            routing: { controller: "GerenciaUsuariosController", templateUrl: "gerenciar-usuarios/" }
        },
        {
            url: prefixUrl+"/alterar-senha", 
            title: "Alterar Senha",
            icon: "fa fa-lock",
            menu: "user",
            clearSession: [], 
            routing: { controller: "GerenciaUsuariosController", templateUrl: "gerenciar-usuarios/senha/" }
        },
        {
            url: prefixUrl+"/sair", 
            title: "Sair",
            icon: "fa fa-arrow-right",
            menu: "user",
            clearSession: [], 
            routing: { controller: "GerenciaUsuariosController", templateUrl: "gerenciar-usuarios/sair/" }
        }
    ];
})();