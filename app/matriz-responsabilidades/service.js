(function(){
    'use strict';

    angular.module("app").service("ResponsabilidadeService", ["ResponsabilidadeMock", function(ResponsabilidadeMock){
        var $ctrl = this;
        $ctrl.getMatrizResponsabilidades = getMatrizResponsabilidades;

        function getMatrizResponsabilidades(){
            return ResponsabilidadeMock.getMatrizResponsabilidades();
        }
    }]);

})();