(function(){
    'use strict';

    angular.module("app").controller("ResponsabilidadeController", ["$scope", "ViewFactory", "ResponsabilidadeService", function($scope, ViewFactory, ResponsabilidadeService){
        var $ctrl = this;
        
        ViewFactory.setTitle("Matriz Responsabilidades");
        ViewFactory.setBreadcrumb([
            { title: "Home", icon: "fa fa-home", url: "/" },
            { title: "Responsabilidades" }
        ]);

        $ctrl.recursoList = [];
        $ctrl.recursoFields = [
            {id: "recurso.nome", label: "Nome" },
            {id: "recurso.papel", label: "Papel" },
            {id: "recurso.horarioTrabalho", label: "Horário de Trabalho" },
            {id: "recurso.telefone", label: "Telefone" },
            {id: "recurso.horasTrabalhadas", label: "Horas Trabalhadas" },
            {id: "recurso.alocacao", label: "Alocação" }
        ];

        $ctrl.stakeholderList = [];
        $ctrl.stakeholderFields = [
            {id: "stakeholder.nome", label: "Nome" },
            {id: "stakeholder.grupo", label: "Grupo" },
            {id: "stakeholder.lotacao", label: "Lotação" },
            {id: "stakeholder.papel", label: "Papel" },
            {id: "stakeholder.telefone", label: "Telefone/Ramal" },
            {id: "stakeholder.email", label: "E-mail" }
        ];

        $ctrl.responsabilidadeRecursoList = [];
        $ctrl.responsabilidadeRecursoFields = [
            {id: "nome", label: "Nome" },
            {id: "papel", label: "Papel" },
            {id: "responsabilidades", label: "Responsabilidades" }
        ];

        $ctrl.responsabilidadeStakeholderList = [];
        $ctrl.responsabilidadeStakeholderFields = [
            {id: "nome", label: "Nome" },
            {id: "papel", label: "Papel" },
            {id: "responsabilidades", label: "Responsabilidades" }
        ];

        $ctrl.matrizResponsabilidades = ViewFactory.getSessionList("matrizResponsabilidades");
        if($ctrl.matrizResponsabilidades.length == 0){
            ResponsabilidadeService.getMatrizResponsabilidades().then(function(response){
                $ctrl.matrizResponsabilidades = response;
                ViewFactory.setSession("matrizResponsabilidades", $ctrl.matrizResponsabilidades);
                init($ctrl.matrizResponsabilidades);
            });
        }else{
            init($ctrl.matrizResponsabilidades)
        }
        
        function init(matrizResponsabilidades){
            $ctrl.recursoList = matrizResponsabilidades.filter(function(item){
                return item.recurso != null
            });
            $ctrl.stakeholderList = matrizResponsabilidades.filter(function(item){
                return item.stakeholder != null
            });
            matrizResponsabilidades.forEach(function(item){
                if(item.recurso){
                    var responsabilidadesRecursos = (angular.isArray(item.recurso.responsabilidades)) ? item.recurso.responsabilidades : [];
                    $ctrl.responsabilidadeRecursoList.push({
                        nome: item.recurso.nome,
                        papel: item.recurso.papel,
                        responsabilidades: responsabilidadesRecursos.join(", "),
                    });
                }else if(item.stakeholder){
                    var responsabilidadesStakeholders = (angular.isArray(item.stakeholder.responsabilidades)) ? item.stakeholder.responsabilidades : [];
                    $ctrl.responsabilidadeStakeholderList.push({
                        nome: item.stakeholder.nome,
                        papel: item.stakeholder.papel,
                        responsabilidades: responsabilidadesStakeholders.join(", "),
                    });
                }
            });
        }

    }]);

})();