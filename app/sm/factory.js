(function(){
    'use strict';

    angular.module("app").factory("SolicitacaoMudancaFactory", [function(){
        function SolicitacaoMudancaFactory(){
            var $ctrl = this;
            $ctrl.orgaoResponsavel = "Tribunal de Justiça do Goiás";
            $ctrl.nContrato = "RP 01/16 EL 10/2016";
            $ctrl.nOrdemServico = "10.XXX";
            $ctrl.projeto = { id: 1, descricao: "MDS – Metodologia de Desenvolvimento de Software" };
            $ctrl.gestor = "Ana Claudia Bastos Loureiro";
            $ctrl.telefoneGestor = "62 3216-2422";
            $ctrl.emailGestor = "acbloureiro@tjgo.jus.br";
            $ctrl.possuiDocumentacao = false;
            $ctrl.nSolicitacaoMudanca = "XXX/"+new Date().getFullYear();
            $ctrl.gerenteProjetos = "William Cesar Cardoso Guiraldelli"
            $ctrl.emailGerenteProjetos = "william.guiraldelli@ctis.com.br"
            $ctrl.telefoneGerenteProjetos = "(61) 98160-2351"
        }
        return SolicitacaoMudancaFactory;
    }]);

})();