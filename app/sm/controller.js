(function(){
    'use strict';

    angular.module("app").controller("SolicitacaoMudancaController", ["$scope", "ViewFactory", "SolicitacaoMudancaFactory", "SolicitacaoMudancaService", "MessagesService", "$uibModal", function($scope, ViewFactory, SolicitacaoMudancaFactory, SolicitacaoMudancaService, MessagesService, $uibModal){
        var $ctrl = this;
        $ctrl.getHiddenSection = getHiddenSection;
        $ctrl.abrirModalHistorico = abrirModalHistorico;

        $ctrl.user = ViewFactory.user;
        ViewFactory.setTitle("Solicitação de Mudança");
        ViewFactory.setBreadcrumb([
            { title: "Home", icon: "fa fa-home", url: "/" },
            { title: "Solicitação de Mudança" },
        ]);

        $ctrl.model = new SolicitacaoMudancaFactory();
        $ctrl.model.solicitante = $ctrl.user.name;
        $ctrl.model.preenchidoPor = $ctrl.user.name;
        $ctrl.model.emailRecurso = $ctrl.user.email;
        
        MessagesService.getMessages().then(function(response){
            $ctrl.messages = response;
        });

        SolicitacaoMudancaService.getTipoSolicitacaoList().then(function(response){
            $ctrl.tipoSolicitacaoList = response;
        });

        SolicitacaoMudancaService.getTipoSustentacaoList().then(function(response){
            $ctrl.tipoSustentacaoList = response;
        });

        SolicitacaoMudancaService.getProjetosList().then(function(response){
            $ctrl.projetosList = response;
        });

        SolicitacaoMudancaService.getTipoServicoList().then(function(response){
            $ctrl.tipoServicoList = response;
            $ctrl.camposServico = [
                {id: "tipoServico", label: "Tipo de Serviço", type: "list", list: $ctrl.tipoServicoList, required: true},
                {id: "periodo", label: "Período de Execução", type: "period", placeholderBegin: "Data Inicial", placeholderFinal: "Data Final", required: true},
                {id: "qtdHoras", label: "Qtde. Horas", type: "number", required: true},
                {id: "descricaoServicos", label: "Descrição dos Serviços Executados", type: "textarea", required: true, class: "col-md-8"},
                {id: "entregas", label: "Entregas", type: "textarea", required: true, class: "col-md-4"}
            ];
        });

        SolicitacaoMudancaService.getPerfilList('teste').then(function(response){
            $ctrl.perfil1List = response;
        });

        SolicitacaoMudancaService.getPerfilList('metodologia').then(function(response){
            $ctrl.perfil2List = response;
        });

        SolicitacaoMudancaService.getNiveisList().then(function(response){
            $ctrl.niveisList = response;
        });

        SolicitacaoMudancaService.getTabelasEnvolvidasList().then(function(response){
            $ctrl.tabelasEnvolvidasList = response;
        });

        function getHiddenSection(){
            var mapping = {
                "Metodologia": "metodologia",
                "Sustentação": "sustentacao",
                "Teste": "teste"
            }
            if($ctrl.model.tipoSolicitacao)
                return 'app/sm/criar/'+mapping[$ctrl.model.tipoSolicitacao.descricao]+'.html';
            else
                return '';
        }

        function abrirModalHistorico(){
            var modalInstance = $uibModal.open({
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'app/sm/criar/modal-historico.html',
                controller: 'ModalHistoricoController',
                controllerAs: '$ctrl',
                size: "lg",
                resolve: {
                  
                }
            });
            modalInstance.result.then(
                function (result) {// Confirm
                    console.log(result);
                }, 
                function () { // Dismiss
                    console.log('Modal Fechada.');
                }
            );
        }
    }]);

})();