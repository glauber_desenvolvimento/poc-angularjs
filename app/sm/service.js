(function(){
    'use strict';

    angular.module("app").service("SolicitacaoMudancaService", ["SolicitacaoMudancaMock", function(SolicitacaoMudancaMock){
        var $ctrl = this;
        $ctrl.getTipoSolicitacaoList = getTipoSolicitacaoList;
        $ctrl.getTipoSustentacaoList = getTipoSustentacaoList;
        $ctrl.getProjetosList = getProjetosList;
        $ctrl.getTipoServicoList = getTipoServicoList;
        $ctrl.getPerfilList = getPerfilList;
        $ctrl.getNiveisList = getNiveisList;
        $ctrl.getTabelasEnvolvidasList = getTabelasEnvolvidasList;

        function getTipoSolicitacaoList(){
            return SolicitacaoMudancaMock.getTipoSolicitacaoList();
        }

        function getTipoSustentacaoList(){
            return SolicitacaoMudancaMock.getTipoSustentacaoList();
        }

        function getProjetosList(){
            return SolicitacaoMudancaMock.getProjetosList();
        }

        function getTipoServicoList(){
            return SolicitacaoMudancaMock.getTipoServicoList();
        }

        function getPerfilList(filtro){
            return SolicitacaoMudancaMock.getPerfilList(filtro);
        }

        function getNiveisList(){
            return SolicitacaoMudancaMock.getNiveisList();
        }

        function getTabelasEnvolvidasList(){
            return SolicitacaoMudancaMock.getTabelasEnvolvidasList();
        }
    }]);

})();