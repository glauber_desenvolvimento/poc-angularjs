(function(){
    'use strict';

    angular.module("app").controller("ModalHistoricoController", ["$uibModalInstance", function($uibModalInstance){
        
        var $ctrl = this;
      
        $ctrl.ok = function () {
            $uibModalInstance.close("Retorno Modal");
        };
      
        $ctrl.cancel = function () {
          $uibModalInstance.dismiss();
        };

    }]);

})();