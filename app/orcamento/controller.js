(function(){
    'use strict';

    angular.module("app").controller("OrcamentoController", ["$scope", "ViewFactory", "TapFactory", "TapService", "OSService", "MessagesService", function($scope, ViewFactory, TapFactory, TapService, OSService, MessagesService){
        var $ctrl = this;

        if(!ViewFactory.getSession('projetoSelected') && ViewFactory.getPath() == "orcamento-projeto"){
            ViewFactory.setPath("/termo-abertura-projeto");
        }

        $ctrl.salvar = salvar;
        $ctrl.editar = editar;
        $ctrl.remover = remover;

        ViewFactory.setTitle("Orçamento & Cronograma do Projeto");
        ViewFactory.setBreadcrumb([
            { title: "Home", icon: "fa fa-home", url: "/" },
            { title: "Projetos", icon: "fa fa-list", url: "/projetos" },
            { title: "Orçamento" },
        ]);

        MessagesService.getMessages().then(function(response){
            $ctrl.messages = response;
        });

        OSService.getTipoOSList().then(function(response){
            $ctrl.tipoOrcamentoList = response;
        });

        TapService.getUnidadeMedidaList().then(function(response){
            $ctrl.unidadeMedidaList = response;
        });
        
        TapService.getOSList().then(function(response){
            $ctrl.OSList = response;
        });
        
        $ctrl.projetosList = ViewFactory.getSessionList("projetosList");
        if($ctrl.projetosList.length == 0){// Se não tiver na da sessão
            TapService.getProjetosList().then(function(response){
                $ctrl.projetosList = response;
                ViewFactory.setSession("projetosList", $ctrl.projetosList);
                init($ctrl.projetosList);
            });
        }else{
            init($ctrl.projetosList);
        }

        function init(projetosList){
            $ctrl.orcamentoList = projetosList.filter(function(projeto){
                return angular.isDefined(projeto.orcamento);
            });
            $ctrl.total = 0;
            for(var i=0; i<$ctrl.orcamentoList.length; i++){
                $ctrl.orcamentoList[i].orcamento.total = $ctrl.orcamentoList[i].orcamento.quantidade * $ctrl.orcamentoList[i].orcamento.valorUnitario;
                $ctrl.total += $ctrl.orcamentoList[i].orcamento.total;
            }
        }

        $ctrl.orcamentoFields = [
            { label: "Descrição", id: "descricaoProjeto" },
            { label: "Tipo", id: "orcamento.tipo", type:"list" },
            { label: "Unidade de Medida", id: "orcamento.unidadeMedida", type:"list" },
            { label: "Quantidade", id: "orcamento.quantidade", type:"number" },
            { label: "Valor Unitário", id: "orcamento.valorUnitario", type:"currency" },
            { label: "Total", id: "orcamento.total", type:"currency" }
        ];

        $ctrl.orcamentoActions = [
            { label: "Editar", class: "btn-info", icon: "edit", callback: $ctrl.editar },
            { label: "Remover", class: "btn-danger", icon: "times", callback: $ctrl.remover },
        ];

        function salvar(){
            if($scope.orcamentoForm.$valid){
                for(var i=0; i<$ctrl.projetosList.length; i++){
                    if(isSameProject(i)){
                        $ctrl.projetosList[i] = $ctrl.model;
                    }
                }
                ViewFactory.setSession("projetosList", $ctrl.projetosList, "/orcamentos");
            }
        }
        
        function editar(item){
            $ctrl.model = item;
            ViewFactory.setSession("projetoSelected", $ctrl.model, "/orcamento-projeto");
        }

        function remover(item){
            $ctrl.model = item;
            delete $ctrl.model.orcamento;
            for(var i=0; i<$ctrl.projetosList.length; i++){
                if(isSameProject(i)){
                    $ctrl.projetosList[i] = $ctrl.model;
                }
            }
            ViewFactory.setSession("projetosList", $ctrl.projetosList, "/orcamentos");
            $ctrl.orcamentoList = $ctrl.projetosList.filter(function(projeto){
                return angular.isDefined(projeto.orcamento);
            });
        }

        function isSameProject(index){
            return (
                $ctrl.projetosList[index].descricaoProjeto == $ctrl.model.descricaoProjeto && 
                $ctrl.projetosList[index].dataTAP.toString() == $ctrl.model.dataTAP.toString() && 
                $ctrl.projetosList[index].mesReferencia.descricao == $ctrl.model.mesReferencia.descricao && 
                $ctrl.projetosList[index].classificacao.descricao == $ctrl.model.classificacao.descricao
            );
        }

        $ctrl.model = ViewFactory.getSessionObject("projetoSelected", new TapFactory());
    }]);

})();