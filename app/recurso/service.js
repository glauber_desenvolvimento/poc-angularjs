(function(){
    'use strict';

    angular.module("app").service("RecursoService", ["RecursoMock", function(RecursoMock){
        var $ctrl = this;
        
        $ctrl.getPapelList = getPapelList;
        $ctrl.getAlocacaoList = getAlocacaoList;
        $ctrl.getEquipeList = getEquipeList;
        $ctrl.getResponsabilidadeList = getResponsabilidadeList;
        
        function getPapelList(){
            return RecursoMock.getPapelList();
        }

        function getAlocacaoList(){
            return RecursoMock.getAlocacaoList();
        }

        function getEquipeList(){
            return RecursoMock.getEquipeList();
        }

        function getResponsabilidadeList(){
            return RecursoMock.getResponsabilidadeList();
        }
    }]);

})();