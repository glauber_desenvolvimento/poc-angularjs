(function(){
    'use strict';

    angular.module("app").controller("RecursoController", ["$scope", "ViewFactory", "RecursoService", "RecursoFactory", "MessagesService", "TapService", "ResponsabilidadeService", function($scope, ViewFactory, RecursoService, RecursoFactory, MessagesService, TapService, ResponsabilidadeService){
        var $ctrl = this;
        

        $ctrl.updateHorarioTrabalho = updateHorarioTrabalho;
        $ctrl.filtrarPapeis = filtrarPapeis;
        $ctrl.salvar = salvar;
        
        ViewFactory.setTitle("Recurso");
        ViewFactory.setBreadcrumb([
            { title: "Home", icon: "fa fa-home", url: "/" },
            { title: "Recurso" },
        ]);

        $ctrl.matrizResponsabilidades = ViewFactory.getSessionList("matrizResponsabilidades");
        if($ctrl.matrizResponsabilidades.length == 0){
            ResponsabilidadeService.getMatrizResponsabilidades().then(function(response){
                $ctrl.matrizResponsabilidades = response;
                ViewFactory.setSession("matrizResponsabilidades", $ctrl.matrizResponsabilidades);
            });
        }

        MessagesService.getMessages().then(function(response){
            $ctrl.messages = response;
        });

        $ctrl.papelFiltradoList = [];

        RecursoService.getPapelList().then(function(response){
            $ctrl.papelList = response;
        });

        RecursoService.getAlocacaoList().then(function(response){
            $ctrl.alocacaoList = response;
        });

        TapService.getProjetosList().then(function(response){
            $ctrl.projetosList = response;
        });

        RecursoService.getEquipeList().then(function(response){
            $ctrl.equipeList = response;
        });

        RecursoService.getResponsabilidadeList().then(function(response){
            $ctrl.responsabilidadeList = response;
        });

        function filtrarPapeis(){
            $ctrl.model.papel = null;
            $ctrl.papelFiltradoList = $ctrl.papelList.filter(function(papel){
                return papel.tipo == $ctrl.model.equipe;
            });
        }

        function updateHorarioTrabalho(){
            if($ctrl.horarioTrabalhoDas && $ctrl.horarioTrabalhoAs && $ctrl.horarioTrabalhoDas.length == 5 && $ctrl.horarioTrabalhoAs.length == 5){
                $ctrl.model.horarioTrabalho = 'Das '+$ctrl.horarioTrabalhoDas+' às '+$ctrl.horarioTrabalhoAs
            }else{
                $ctrl.model.horarioTrabalho = '';
            }
        }

        function salvar(){
            if($scope.recursoForm.$valid){
                $ctrl.matrizResponsabilidades.unshift({recurso: $ctrl.model, stakeholder: null});
                ViewFactory.setSession("matrizResponsabilidades", $ctrl.matrizResponsabilidades, "/matriz-responsabilidades");
            }
        }

        $ctrl.model = new RecursoFactory();
        
    }]);

})();