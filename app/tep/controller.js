(function(){
    'use strict';

    angular.module("app").controller("TepController", ["$scope", "ViewFactory", "TapFactory", "TapService", "MessagesService", function($scope, ViewFactory, TapFactory, TapService, MessagesService){
        var $ctrl = this;

        if(!ViewFactory.getSession('projetoSelected') && ViewFactory.getPath() == "termo-encerramento-projeto"){
            ViewFactory.setPath("/termo-abertura-projeto");
        }

        $ctrl.salvar = salvar;

        ViewFactory.setTitle("Termo de Encerramento de Projeto");
        ViewFactory.setBreadcrumb([
            { title: "Home", icon: "fa fa-home", url: "/" },
            { title: "Projetos", icon: "fa fa-list", url: "/projetos" },
            { title: "TEP" },
        ]);

        MessagesService.getMessages().then(function(response){
            $ctrl.messages = response;
        });

        $ctrl.projetosList = ViewFactory.getSessionList("projetosList");
        if($ctrl.projetosList.length == 0){// Se não tiver na da sessão
            TapService.getProjetosList().then(function(response){
                $ctrl.projetosList = response;
                ViewFactory.setSession("projetosList", $ctrl.projetosList);
            });
        }

        TapService.getAvaliacaoList().then(function(response){
            $ctrl.avaliacaoList = response;
        });

        TapService.getOSList().then(function(response){
            $ctrl.OSList = response;
            $ctrl.camposEntregas = [
                { label: 'OS', id: 'os', type: 'list', list: $ctrl.OSList, required: true },
                { label: 'Data', id: 'data', type: "date", required: true },
                { label: 'Url Entrega', id: 'urlEntrega', class: "col-md-8", required: true },
                { label: 'Responsável', id: 'responsavel', required: true },
            ];
        });

        $ctrl.camposResponsaveis = [
            { label: 'Nome', id: 'nome', required: true },
            { label: 'Contato', id: 'contato', required: true }
        ];

        function salvar(){
            if($scope.tepForm.$valid){
                for(var i=0; i<$ctrl.projetosList.length; i++){
                    if(isSameProject(i)){
                        $ctrl.projetosList[i] = $ctrl.model;
                    }
                }
                ViewFactory.setSession("projetosList", $ctrl.projetosList, "/projetos");
            }
        }

        function isSameProject(index){
            return (
                $ctrl.projetosList[index].descricaoProjeto == $ctrl.model.descricaoProjeto && 
                $ctrl.projetosList[index].dataTAP.toString() == $ctrl.model.dataTAP.toString() && 
                $ctrl.projetosList[index].mesReferencia.descricao == $ctrl.model.mesReferencia.descricao && 
                $ctrl.projetosList[index].classificacao.descricao == $ctrl.model.classificacao.descricao
            );
        }

        $ctrl.model = ViewFactory.getSessionObject("projetoSelected", new TapFactory());
    }]);

})();