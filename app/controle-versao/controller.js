(function(){
    'use strict';

    angular.module("app").controller("ControleVersaoController", ["$scope", "ViewFactory", "TapFactory", "TapService", "MessagesService", function($scope, ViewFactory, TapFactory, TapService, MessagesService){
        var $ctrl = this;

        if(!ViewFactory.getSession('projetoSelected') && ViewFactory.getPath() == "controle-versao"){
            ViewFactory.setPath("/termo-abertura-projeto");
        }

        $ctrl.salvar = salvar;
        $ctrl.editar = editar;
        $ctrl.remover = remover;

        ViewFactory.setTitle("Controle de Versão");
        ViewFactory.setBreadcrumb([
            { title: "Home", icon: "fa fa-home", url: "/" },
            { title: "Projetos", icon: "fa fa-list", url: "/projetos" },
            { title: "Versão" },
        ]);

        MessagesService.getMessages().then(function(response){
            $ctrl.messages = response;
        });

        $ctrl.projetosList = ViewFactory.getSessionList("projetosList");
        if($ctrl.projetosList.length == 0){// Se não tiver na da sessão
            TapService.getProjetosList().then(function(response){
                $ctrl.projetosList = response;
                ViewFactory.setSession("projetosList", $ctrl.projetosList);
                $ctrl.controleVersaoList = $ctrl.projetosList.filter(function(projeto){
                    return angular.isDefined(projeto.controleVersao);
                });
            });
        }else{
            $ctrl.controleVersaoList = $ctrl.projetosList.filter(function(projeto){
                return angular.isDefined(projeto.controleVersao);
            });
        }

        $ctrl.controleVersaoFields = [
            { label: "Descrição", id: "descricaoProjeto" },
            { label: "Data", id: "controleVersao.data", type: "date" },
            { label: "Versão Anterior", id: "controleVersao.versaoAnterior" },
            { label: "Versão Atual", id: "controleVersao.versaoAtual" }
        ];

        $ctrl.controleVersaoActions = [
            { label: "Editar", class: "btn-info", icon: "edit", callback: $ctrl.editar },
            { label: "Remover", class: "btn-danger", icon: "times", callback: $ctrl.remover },
        ];

        function salvar(){
            if($scope.controleVersaoForm.$valid){
                for(var i=0; i<$ctrl.projetosList.length; i++){
                    if(isSameProject(i)){
                        $ctrl.projetosList[i] = $ctrl.model;
                    }
                }
                ViewFactory.setSession("projetosList", $ctrl.projetosList, "/controles-versoes");
            }
        }
        
        function editar(item){
            $ctrl.model = item;
            ViewFactory.setSession("projetoSelected", $ctrl.model, "/controle-versao");
        }

        function remover(item){
            $ctrl.model = item;
            delete $ctrl.model.controleVersao;
            for(var i=0; i<$ctrl.projetosList.length; i++){
                if(isSameProject(i)){
                    $ctrl.projetosList[i] = $ctrl.model;
                }
            }
            ViewFactory.setSession("projetosList", $ctrl.projetosList, "/controles-versoes");
            $ctrl.controleVersaoList = $ctrl.projetosList.filter(function(projeto){
                return angular.isDefined(projeto.controleVersao);
            });
        }

        function isSameProject(index){
            return (
                $ctrl.projetosList[index].descricaoProjeto == $ctrl.model.descricaoProjeto && 
                $ctrl.projetosList[index].dataTAP.toString() == $ctrl.model.dataTAP.toString() && 
                $ctrl.projetosList[index].mesReferencia.descricao == $ctrl.model.mesReferencia.descricao && 
                $ctrl.projetosList[index].classificacao.descricao == $ctrl.model.classificacao.descricao
            );
        }

        $ctrl.model = ViewFactory.getSessionObject("projetoSelected", new TapFactory());
    }]);

})();