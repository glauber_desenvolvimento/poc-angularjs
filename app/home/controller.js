(function(){
    'use strict';

    angular.module("app").controller("HomeController", ["$scope", "HomeFactory", "ViewFactory", function($scope, HomeFactory, ViewFactory){
        var $ctrl = this;

        ViewFactory.init();

        $ctrl.lines = new HomeFactory();
        $ctrl.lines.setLabels(["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho"]);
        $ctrl.lines.setSeries(['Series A', 'Series B']);
        $ctrl.lines.setData([
          [65, 59, 80, 81, 56, 55, 40],
          [28, 48, 40, 19, 86, 27, 90]
        ]);

        $ctrl.pie = new HomeFactory();
        $ctrl.pie.setLabels(["Download Sales", "In-Store Sales", "Mail-Order Sales"]);
        $ctrl.pie.setData([300, 500, 100]);

        $ctrl.bars = new HomeFactory();
        $ctrl.bars.setLabels(['2012', '2013', '2014', '2015', '2016', '2017', '2018']);
        $ctrl.bars.setSeries(['Series A', 'Series B']);
        $ctrl.bars.setData([
          [65, 59, 80, 81, 56, 55, 40],
          [28, 48, 40, 19, 86, 27, 90]
        ]);

        $ctrl.horizontalBars = new HomeFactory();
        $ctrl.horizontalBars.setLabels(['1º Semana', '2º Semana', '3º Semana', '4º Semana']);
        $ctrl.horizontalBars.setSeries(['Mês atual']);
        $ctrl.horizontalBars.setData([
          [70, 81, 56, 90]
        ]);
    }]);

})();