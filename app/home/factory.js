(function(){
    'use strict';

    angular.module("app").factory("HomeFactory", function(){
        function HomeFactory(){
            var $ctrl = this;

            $ctrl.setLabels = function(labels){
              $ctrl.labels = labels;
            }

            $ctrl.setSeries = function(series){
              $ctrl.series = series;
            }

            $ctrl.setData = function(data){
              $ctrl.data = data;
            }

        }
        return HomeFactory;
    });

})();