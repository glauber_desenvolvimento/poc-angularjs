(function(){
    'use strict';

    angular.module("app").service("RiscoService", ["RiscoMock", function(RiscoMock){
        var $ctrl = this;
        $ctrl.getCategoriaList = getCategoriaList;
        $ctrl.getSituacaoList = getSituacaoList;

        function getCategoriaList(){
            return RiscoMock.getCategoriaList();
        }

        function getSituacaoList(){
            return RiscoMock.getSituacaoList();
        }
    }]);

})();