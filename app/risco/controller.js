(function(){
    'use strict';

    angular.module("app").controller("RiscoController", ["$scope", "ViewFactory", "TapFactory", "TapService", "MessagesService", "RiscoService", function($scope, ViewFactory, TapFactory, TapService, MessagesService, RiscoService){
        var $ctrl = this;

        if(!ViewFactory.getSession('projetoSelected') && ViewFactory.getPath() == "risco"){
            ViewFactory.setPath("/termo-abertura-projeto");
        }

        $ctrl.salvar = salvar;
        $ctrl.editar = editar;
        $ctrl.remover = remover;

        ViewFactory.setTitle("Plano de Riscos");
        ViewFactory.setBreadcrumb([
            { title: "Home", icon: "fa fa-home", url: "/" },
            { title: "Projetos", icon: "fa fa-list", url: "/projetos" },
            { title: "Riscos" },
        ]);

        MessagesService.getMessages().then(function(response){
            $ctrl.messages = response;
        });
        
        $ctrl.planoRiscoFields = [
            { label: "Descrição", id: "descricaoProjeto" },
            { label: "Preparado Por", id: "planoRisco.preparadoPor" },
            { label: "Aprovado Por", id: "planoRisco.aprovadoPor" },
            { label: "Número Doc.", id: "planoRisco.numeroDoc"},
            { label: "Data", id: "planoRisco.data", type: "date" },
            { label: "Versão", id: "planoRisco.versao" },
        ];

        $ctrl.planoRiscoActions = [
            { label: "Editar", class: "btn-info", icon: "edit", callback: $ctrl.editar },
            { label: "Remover", class: "btn-danger", icon: "times", callback: $ctrl.remover },
        ];

        $ctrl.projetosList = ViewFactory.getSessionList("projetosList");
        if($ctrl.projetosList.length == 0){// Se não tiver na da sessão
            TapService.getProjetosList().then(function(response){
                $ctrl.projetosList = response;
                ViewFactory.setSession("projetosList", $ctrl.projetosList);
                $ctrl.planoRiscoList = $ctrl.projetosList.filter(function(projeto){
                    return angular.isDefined(projeto.planoRisco);
                });
            });
        }else{
            $ctrl.planoRiscoList = $ctrl.projetosList.filter(function(projeto){
                return angular.isDefined(projeto.planoRisco);
            });
        }

        RiscoService.getCategoriaList().then(function(response){
            $ctrl.categoriaList = response;
            $ctrl.camposIdentificacao = [
                { label: "Data", id: "data", type: "date", required: true },
                { label: "ID", id: "id", required: true },
                { label: "Descrição", id: "descricao", type: "textarea", class:"col-md-8", required: true },
                { label: "Categoria", id: "categoria", class:"col-md-4 clearfix", type: "list", list: $ctrl.categoriaList, required: true },
                { label: "Probabilidade", id: "probabilidade" },
                { label: "Impacto Previsto", id: "impactoPrevisto" },
                { label: "Concequências", type: "textarea", class:"col-md-8", id: "concequencias" },
                { label: "Exposição", id: "exposicao", class:"col-md-4 clearfix" },
                { label: "Indicador", id: "indicador", type: "number"  },
                { label: "Valor Referência", id: "valorReferencia", type: "number" }
            ];
        });

        RiscoService.getSituacaoList().then(function(response){
            $ctrl.situacaoList = response;
            $ctrl.camposResposta = [
                { label: "Resposta", id: "resposta", required: true },
                { label: "Ação", id: "acao", type: "textarea", class: "col-md-8 clearfix", required: true },
                { label: "Estratégia para ação", id: "estrategiaAcao", class: "col-md-4 clearfix", required: true },
                { label: "Data para ação", id: "dataAcao", type: "date", required: true },
                { label: "Classificação", id: "classificacao", class: "col-md-4" },
                { label: "Impacto", id: "impacto" },
                { label: "Situação", id: "situacao", type: "list", list: $ctrl.situacaoList,  }
            ];
        });

        function salvar(){
            if($scope.planoRiscoForm.$valid){
                for(var i=0; i<$ctrl.projetosList.length; i++){
                    if(isSameProject(i)){
                        $ctrl.projetosList[i] = $ctrl.model;
                    }
                }
                ViewFactory.setSession("projetosList", $ctrl.projetosList, "/riscos");
            }
        }
        
        function editar(item){
            $ctrl.model = item;
            ViewFactory.setSession("projetoSelected", $ctrl.model, "/risco");
        }

        function remover(item){
            $ctrl.model = item;
            delete $ctrl.model.planoRisco;
            for(var i=0; i<$ctrl.projetosList.length; i++){
                if(isSameProject(i)){
                    $ctrl.projetosList[i] = $ctrl.model;
                }
            }
            ViewFactory.setSession("projetosList", $ctrl.projetosList, "/riscos");
            $ctrl.planoRiscoList = $ctrl.projetosList.filter(function(projeto){
                return angular.isDefined(projeto.planoRisco);
            });
        }

        function isSameProject(index){
            return (
                $ctrl.projetosList[index].descricaoProjeto == $ctrl.model.descricaoProjeto && 
                $ctrl.projetosList[index].dataTAP.toString() == $ctrl.model.dataTAP.toString() && 
                $ctrl.projetosList[index].mesReferencia.descricao == $ctrl.model.mesReferencia.descricao && 
                $ctrl.projetosList[index].classificacao.descricao == $ctrl.model.classificacao.descricao
            );
        }

        $ctrl.model = ViewFactory.getSessionObject("projetoSelected", new TapFactory());
        $ctrl.usuarioLogado = ViewFactory.user.name;

        

    }]);

})();