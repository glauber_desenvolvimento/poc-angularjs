(function(){
    'use strict';

    angular.module("app").service("PlanoComunicacaoService", ["PlanoComunicacaoMock", function(PlanoComunicacaoMock){
        var $ctrl = this;
        $ctrl.getResponsavelList = getResponsavelList;
        $ctrl.getMetodologiaList = getMetodologiaList;
        $ctrl.getPublicoEnvolvidoList = getPublicoEnvolvidoList;
        $ctrl.getFrequenciaList = getFrequenciaList;

        function getResponsavelList(){
            return PlanoComunicacaoMock.getResponsavelList();
        }

        function getMetodologiaList(){
            return PlanoComunicacaoMock.getMetodologiaList();
        }

        function getPublicoEnvolvidoList(){
            return PlanoComunicacaoMock.getPublicoEnvolvidoList();
        }

        function getFrequenciaList(){
            return PlanoComunicacaoMock.getFrequenciaList();
        }

    }]);

})();