(function(){
    'use strict';

    angular.module("app").controller("PlanoComunicacaoController", ["$scope", "ViewFactory", "TapFactory", "TapService", "MessagesService", "PlanoComunicacaoService", function($scope, ViewFactory, TapFactory, TapService, MessagesService, PlanoComunicacaoService){
        var $ctrl = this;

        if(!ViewFactory.getSession('projetoSelected') && ViewFactory.getPath() == "plano-comunicacao"){
            ViewFactory.setPath("/termo-abertura-projeto");
        }

        $ctrl.salvar = salvar;
        $ctrl.editar = editar;
        $ctrl.remover = remover;

        ViewFactory.setTitle("Plano de Comunicação");
        ViewFactory.setBreadcrumb([
            { title: "Home", icon: "fa fa-home", url: "/" },
            { title: "Projetos", icon: "fa fa-list", url: "/projetos" },
            { title: "Comunicação" },
        ]);

        MessagesService.getMessages().then(function(response){
            $ctrl.messages = response;
        });

        $ctrl.projetosList = ViewFactory.getSessionList("projetosList");
        if($ctrl.projetosList.length == 0){// Se não tiver na da sessão
            TapService.getProjetosList().then(function(response){
                $ctrl.projetosList = response;
                ViewFactory.setSession("projetosList", $ctrl.projetosList);
                $ctrl.planoComunicacaoList = $ctrl.projetosList.filter(function(projeto){
                    return angular.isDefined(projeto.planoComunicacao);
                });
            });
        }else{
            $ctrl.planoComunicacaoList = $ctrl.projetosList.filter(function(projeto){
                return angular.isDefined(projeto.planoComunicacao);
            });
        }

        PlanoComunicacaoService.getPublicoEnvolvidoList().then(function(response){
            $ctrl.publicoEnvolvidoList = response;
        });

        PlanoComunicacaoService.getResponsavelList().then(function(response){
            $ctrl.responsavelList = response;
        });

        PlanoComunicacaoService.getMetodologiaList().then(function(response){
            $ctrl.metodologiaList = response;
        });

        PlanoComunicacaoService.getFrequenciaList().then(function(response){
            $ctrl.frequenciaList = response;
        });

        $ctrl.planoComunicacaoFields = [
            { label: "Descrição", id: "descricaoProjeto" },
            { label: "ID", id: "planoComunicacao.id" },
            { label: "Objetivo", id: "planoComunicacao.objetivo" },
            { label: "Frequência", id: "planoComunicacao.frequencia", type: "list" },
            { label: "Duração", id: "planoComunicacao.duracao" }
        ];

        $ctrl.planoComunicacaoActions = [
            { label: "Editar", class: "btn-info", icon: "edit", callback: $ctrl.editar },
            { label: "Remover", class: "btn-danger", icon: "times", callback: $ctrl.remover },
        ];

        function salvar(){
            if($scope.planoComunicacaoForm.$valid){
                for(var i=0; i<$ctrl.projetosList.length; i++){
                    if(isSameProject(i)){
                        $ctrl.projetosList[i] = $ctrl.model;
                    }
                }
                ViewFactory.setSession("projetosList", $ctrl.projetosList, "/planos-comunicacoes");
            }
        }
        
        function editar(item){
            $ctrl.model = item;
            ViewFactory.setSession("projetoSelected", $ctrl.model, "/plano-comunicacao");
        }

        function remover(item){
            $ctrl.model = item;
            delete $ctrl.model.planoComunicacao;
            for(var i=0; i<$ctrl.projetosList.length; i++){
                if(isSameProject(i)){
                    $ctrl.projetosList[i] = $ctrl.model;
                }
            }
            ViewFactory.setSession("projetosList", $ctrl.projetosList, "/planos-comunicacoes");
            $ctrl.planoComunicacaoList = $ctrl.projetosList.filter(function(projeto){
                return angular.isDefined(projeto.planoComunicacao);
            });
        }

        function isSameProject(index){
            return (
                $ctrl.projetosList[index].descricaoProjeto == $ctrl.model.descricaoProjeto && 
                $ctrl.projetosList[index].dataTAP.toString() == $ctrl.model.dataTAP.toString() && 
                $ctrl.projetosList[index].mesReferencia.descricao == $ctrl.model.mesReferencia.descricao && 
                $ctrl.projetosList[index].classificacao.descricao == $ctrl.model.classificacao.descricao
            );
        }

        $ctrl.model = ViewFactory.getSessionObject("projetoSelected", new TapFactory());
    }]);

})();