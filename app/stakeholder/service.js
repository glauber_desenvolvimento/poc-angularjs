(function(){
    'use strict';

    angular.module("app").service("StakeholderService", ["StakeholderMock", function(StakeholderMock){
        var $ctrl = this;
        
        $ctrl.getGrupoList = getGrupoList;
        $ctrl.getLotacaoList = getLotacaoList;
        $ctrl.getPapelList = getPapelList;
        $ctrl.getEquipeList = getEquipeList;
        $ctrl.getResponsabilidadeList = getResponsabilidadeList;
        
        function getGrupoList(){
            return StakeholderMock.getGrupoList();
        }

        function getLotacaoList(){
            return StakeholderMock.getLotacaoList();
        }

        function getPapelList(){
            return StakeholderMock.getPapelList();
        }

        function getEquipeList(){
            return StakeholderMock.getEquipeList();
        }

        function getResponsabilidadeList(){
            return StakeholderMock.getResponsabilidadeList();
        }

    }]);

})();