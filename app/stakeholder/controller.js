(function(){
    'use strict';

    angular.module("app").controller("StakeholderController", ["$scope", "ViewFactory", "StakeholderService", "StakeholderFactory", "MessagesService", "TapService", "ResponsabilidadeService", function($scope, ViewFactory, StakeholderService, StakeholderFactory, MessagesService, TapService, ResponsabilidadeService){
        var $ctrl = this;

        $ctrl.filtrarPapeis = filtrarPapeis;
        $ctrl.salvar = salvar;
        
        ViewFactory.setTitle("Mapeamento de Stakeholders");
        ViewFactory.setBreadcrumb([
            { title: "Home", icon: "fa fa-home", url: "/" },
            { title: "Stakeholder" },
        ]);
        
        $ctrl.matrizResponsabilidades = ViewFactory.getSessionList("matrizResponsabilidades");
        if($ctrl.matrizResponsabilidades.length == 0){
            ResponsabilidadeService.getMatrizResponsabilidades().then(function(response){
                $ctrl.matrizResponsabilidades = response;
                ViewFactory.setSession("matrizResponsabilidades", $ctrl.matrizResponsabilidades);
            });
        }

        MessagesService.getMessages().then(function(response){
            $ctrl.messages = response;
        });

        TapService.getProjetosList().then(function(response){
            $ctrl.projetosList = response;
        });

        StakeholderService.getLotacaoList().then(function(response){
            $ctrl.lotacaoList = response;
        });

        StakeholderService.getGrupoList().then(function(response){
            $ctrl.grupoList = response;
        });

        $ctrl.papelFiltradoList = [];

        StakeholderService.getPapelList().then(function(response){
            $ctrl.papelList = response;
        });

        StakeholderService.getEquipeList().then(function(response){
            $ctrl.equipeList = response;
        });

        StakeholderService.getResponsabilidadeList().then(function(response){
            $ctrl.responsabilidadeList = response;
        });

        $ctrl.model = new StakeholderFactory();

        function filtrarPapeis(){
            $ctrl.model.papel = null;
            $ctrl.papelFiltradoList = $ctrl.papelList.filter(function(papel){
                return papel.tipo == $ctrl.model.equipe;
            });
        }

        function salvar(){
            if($scope.stakeholderForm.$valid){
                $ctrl.matrizResponsabilidades.unshift({stakeholder: $ctrl.model, recurso: null});
                ViewFactory.setSession("matrizResponsabilidades", $ctrl.matrizResponsabilidades, "/matriz-responsabilidades");
            }
        }

    }]);

})();