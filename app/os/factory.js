(function(){
    'use strict';
    
    angular.module("app").factory("OSFactory", function(){
        function RecebimentoDefinitivoFactory(){
            var $ctrl = this;
            $ctrl.orgaoResponsavel = "Tribunal de Justiça do Goiás";
            $ctrl.nContrato = "RP 01/16 EL 10/2016";
            $ctrl.gerenteProjetos = "William Cesar Cardoso Guiraldelli";
            $ctrl.emailGerenteProjetos = "william.guiraldelli@ctis.com.br";
            $ctrl.telefoneGerenteProjetos = "(61) 98160-2351";
        }
        return RecebimentoDefinitivoFactory;
    });

})();