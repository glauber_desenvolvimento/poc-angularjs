(function(){
    'use strict';

    angular.module("app").service("OSService", ["OSMock", function(OSMock){
        var $ctrl = this;
        $ctrl.getTipoOSList = getTipoOSList;
        $ctrl.getPerfilList = getPerfilList;
        $ctrl.getAtoresList = getAtoresList;
        $ctrl.getRecursosList = getRecursosList;
        $ctrl.getTipoArquivoList = getTipoArquivoList;
        
        function getTipoOSList(){
            return OSMock.getTipoOSList();
        }

        function getPerfilList(){
            return OSMock.getPerfilList();
        }

        function getAtoresList(filtro){
            return OSMock.getAtoresList(filtro);
        }

        function getRecursosList(){
            return OSMock.getRecursosList();
        }

        function getTipoArquivoList(){
            return OSMock.getTipoArquivoList();
        }

    }]);

})();