(function(){
    'use strict';

    angular.module("app").controller("AberturaOSController", ["$scope", "ViewFactory", "OSService", "OSFactory", "SolicitacaoMudancaService", "MessagesService", function($scope, ViewFactory, OSService, OSFactory, SolicitacaoMudancaService, MessagesService){
        var $ctrl = this;
        $ctrl.getHiddenSection = getHiddenSection;

        $ctrl.user = ViewFactory.user;
        var title = "Recebimento Definitivo";
        $ctrl.labelTipo = "Tipo Ordem Serviço";
        if(ViewFactory.getPath() == "abertura-os"){
            title = "Abertura de OS";
            $ctrl.labelTipo = "Tipo Termo de Recebimento";
        }

        ViewFactory.setTitle(title);
        ViewFactory.setBreadcrumb([
            { title: "Home", icon: "fa fa-home", url: "/" },
            { title: title },
        ]);
        
        $ctrl.model = new OSFactory();
        $ctrl.model.solicitante = ViewFactory.user.name;
        $ctrl.model.emailSolicitante = ViewFactory.user.email;
        $ctrl.model.telefoneSolicitante = ViewFactory.user.telefone;

        $ctrl.camposRelacaoOrdemServico = [
            {id: "demandaJira", label: "Demanda Jira", type: "text", required: true},
            {id: "numeroOS", label: "Número OS", type: "text", required: true},
            {id: "dataInicio", label: "Data Início", type: "date", required: true, class: "col-md-4 clearfix"},
            {id: "dataTermino", label: "Data Término", type: "date", required: true},
            {id: "evidenciaOS", label: "Evidência OS", type: "textarea", required: true, class: "col-md-8"}
        ];

        $ctrl.camposRelacaoOSSustentacao = [
            {id: "demandaJira", label: "Demanda Jira", type: "text", required: true},
            {id: "numeroOS", label: "Número OS", type: "text", required: true},
            {id: "dataInicio", label: "Data Início", type: "date", required: true, class: "col-md-4 clearfix"},
            {id: "dataTermino", label: "Data Término", type: "date", required: true},
            {id: "dataEntrega", label: "Data Entrega", type: "date", required: true},
            {id: "evidenciaOS", label: "Evidência OS", type: "textarea", required: true, class: "col-md-8"}
        ];

        $ctrl.camposAnexos = [
            {id: "nomeArquivo", label: "Nome do Arquivo", type: "text", required: true},
            {id: "descricao", label: "Descrição", type: "textarea", required: true, class:"col-md-4 clearfix"},
            {id: "evidencia", label: "Evidência (SVN/JIRA)", type: "textarea", required: true},
            {id: "arquivo", label: "Arquivo", type: "file", class:"col-md-8"},
        ];

        $ctrl.camposAssinaturasTecnicos = [
            {id: "nomeTecnico", label: "Nome do Técnico", type: "text", required: true},
            {id: "data", label: "Data", type: "date", required: true},
            {id: "assinatura", label: "Confirmação Assinatura", type: "checkbox", required: true}
        ];

        $ctrl.camposAssinaturasGestores = [
            {id: "nomeGestor", label: "Nome do Gestor", type: "text", required: true},
            {id: "data", label: "Data", type: "date", required: true},
            {id: "assinatura", label: "Confirmação Assinatura", type: "checkbox", required: true}
        ];

        MessagesService.getMessages().then(function(response){
            $ctrl.messages = response;
        });

        OSService.getTipoOSList().then(function(response){
            $ctrl.tipoList = response;
        });

        OSService.getPerfilList().then(function(response){
            $ctrl.perfilList = response;
            $ctrl.camposEquipe = [
                {id: "qtd", label: "Qtde", type: "number", required: true},
                {id: "equipe", label: "Equipe", type: "list", list: $ctrl.perfilList, required: true},
                {id: "periodo", label: "Período", type: "period", placeholderBegin: "Data Inicial", placeholderFinal: "Data Final", required: true},
                {id: "diasUteis", label: "Dias Uteis", type: "number", required: true},
                {id: "esforco", label: "Esforço", type: "number", required: true}
            ];
        });

        OSService.getAtoresList('novosProjetos').then(function(response){
            $ctrl.atoresProjetosList = response;
        });

        OSService.getAtoresList('teste').then(function(response){
            $ctrl.atoresTesteList = response;
            $ctrl.camposAtor = [
                {id: "ator", label: "Ator", type: "list", list: $ctrl.atoresTesteList, required: true},
                {id: "quantidade", label: "Quantidade", type: "number", required: true}
            ];
        });

        OSService.getRecursosList().then(function(response){
            $ctrl.recursosList = response;
        });

        OSService.getTipoArquivoList().then(function(response){
            $ctrl.tipoArquivoList = response;
            $ctrl.camposAnexosSustentacao = [
                {id: "nomeArquivo", label: "Demanda Jira", type: "text", required: true},
                {id: "tipo", label: "Tipo", type: "list", list: $ctrl.tipoArquivoList, required: true},
                {id: "evidencia", label: "Evidência", type: "textarea", required: true, class:"col-md-8"},
                {id: "arquivo", label: "Arquivo", type: "file", class:"col-md-8"},
            ];
        });

        SolicitacaoMudancaService.getProjetosList().then(function(response){
            $ctrl.projetosList = response;
        });

        SolicitacaoMudancaService.getTipoServicoList().then(function(response){
            $ctrl.tipoServicoList = response;
            $ctrl.camposServico = [
                {id: "tipoServico", label: "Tipo de Serviço", type: "list", list: $ctrl.tipoServicoList, required: true},
                {id: "periodo", label: "Período de Execução", type: "period", placeholderBegin: "Data Inicial", placeholderFinal: "Data Final", required: true},
                {id: "qtdHoras", label: "Qtde. Horas", type: "number", required: true},
                {id: "descricaoServicos", label: "Descrição dos Serviços Executados", type: "textarea", required: true, class: "col-md-8"},
                {id: "entregas", label: "Entregas", type: "textarea", required: true, class: "col-md-4"}
            ];
        });

        function getHiddenSection(){
            var mapping = {
                "Novos Projetos": "novos-projetos",
                "Metodologia": "metodologia",
                "Teste": "teste",
                "Metodologia, Sustentação Evolutiva e Teste": "sustentacao"
            };
            if($ctrl.model.tipo)
                return 'app/os/criar/'+mapping[$ctrl.model.tipo.descricao]+'.html';
            else
                return '';
        }

    }]);

})();