(function(){
    'use strict';

    angular.module("app").controller("CronogramaInicialController", ["$scope", "ViewFactory", "TapService", function($scope, ViewFactory, TapService){
        var $ctrl = this;

        $ctrl.salvar = salvar;
        $ctrl.editar = editar;
        $ctrl.remover = remover;

        ViewFactory.setTitle("Cronograma do Projeto");
        ViewFactory.setBreadcrumb([
            { title: "Home", icon: "fa fa-home", url: "/" },
            { title: "Projetos", icon: "fa fa-list", url: "/projetos" },
            { title: "Cronograma" },
        ]);
        

        $ctrl.projetosList = ViewFactory.getSessionList("projetosList");
        if($ctrl.projetosList.length == 0){// Se não tiver na da sessão
            TapService.getProjetosList().then(function(response){
                $ctrl.projetosList = response;
                ViewFactory.setSession("projetosList", $ctrl.projetosList);
                $ctrl.cronogramaList = $ctrl.projetosList.filter(function(cronograma){
                    return angular.isDefined(cronograma.cronograma);
                });
            });
        }else{
            $ctrl.cronogramaList = $ctrl.projetosList.filter(function(cronograma){
                return angular.isDefined(cronograma.cronograma);
            });
        }

        $ctrl.cronogramaFields = [
            { label: "OS", id: "cronograma.os", type: "list" },
            { label: "Descrição", id: "descricaoProjeto" },
            { label: "Data Prevista Inicial", id: "cronograma.dataPrevistaInicial", type:"date" },
            { label: "Data Prevista Entrega", id: "cronograma.dataPrevistaEntrega", type:"date" }
        ];

        $ctrl.cronogramaActions = [
            { label: "Editar", class: "btn-info", icon: "edit", callback: $ctrl.editar },
            { label: "Remover", class: "btn-danger", icon: "times", callback: $ctrl.remover },
        ];

        function salvar(){
            if($scope.orcamentoForm.$valid){
                for(var i=0; i<$ctrl.projetosList.length; i++){
                    if(isSameProject(i)){
                        alert();
                        $ctrl.projetosList[i] = $ctrl.model;
                    }
                }
                ViewFactory.setSession("projetosList", $ctrl.projetosList, "/orcamentos");
            }
        }
        
        function editar(item){
            $ctrl.model = item;
            ViewFactory.setSession("projetoSelected", $ctrl.model, "/orcamento-projeto");
        }

        function remover(item){
            $ctrl.model = item;
            delete $ctrl.model.cronograma;
            for(var i=0; i<$ctrl.projetosList.length; i++){
                if(isSameProject(i)){
                    $ctrl.projetosList[i] = $ctrl.model;
                }
            }
            ViewFactory.setSession("projetosList", $ctrl.projetosList, "/cronograma-inicial");
            $ctrl.cronogramaList = $ctrl.projetosList.filter(function(projeto){
                return angular.isDefined(projeto.cronograma);
            });
        }

        function isSameProject(index){
            return (
                $ctrl.projetosList[index].descricaoProjeto == $ctrl.model.descricaoProjeto && 
                $ctrl.projetosList[index].dataTAP.toString() == $ctrl.model.dataTAP.toString() && 
                $ctrl.projetosList[index].mesReferencia.descricao == $ctrl.model.mesReferencia.descricao && 
                $ctrl.projetosList[index].classificacao.descricao == $ctrl.model.classificacao.descricao
            );
        }

    }]);

})();