(function(){
    'use strict';

    angular.module("app").service("TapService", ["TapMock", function(TapMock){
        var $ctrl = this;
        $ctrl.getMesReferenciaList = getMesReferenciaList;
        $ctrl.getClassificacaoList = getClassificacaoList;
        $ctrl.getUnidadeMedidaList = getUnidadeMedidaList;
        $ctrl.getProjetosList = getProjetosList;
        $ctrl.getPapeisList = getPapeisList;
        $ctrl.getAvaliacaoList = getAvaliacaoList;
        $ctrl.getOSList = getOSList;

        function getMesReferenciaList(){
            return TapMock.getMesReferenciaList();
        }

        function getClassificacaoList(){
            return TapMock.getClassificacaoList();
        }

        function getUnidadeMedidaList(){
            return TapMock.getUnidadeMedidaList();
        }

        function getProjetosList(){
            return TapMock.getProjetosList();
        }

        function getPapeisList(){
            return TapMock.getPapeisList();
        }

        function getAvaliacaoList(){
            return TapMock.getAvaliacaoList();
        }

        function getOSList(){
            return TapMock.getOSList();
        }

    }]);

})();