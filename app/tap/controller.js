(function(){
    'use strict';

    angular.module("app").controller("TapController", ["$scope", "TapFactory", "TapService", "MessagesService", "ViewFactory", function($scope, TapFactory, TapService, MessagesService, ViewFactory){
        var $ctrl = this;

        var index = ViewFactory.getSession("index", -1);
        $ctrl.salvar = salvar;
        $ctrl.editar = editar;
        $ctrl.remover = remover;
        $ctrl.anexar = anexar;

        if(ViewFactory.getPath() == "projetos"){
            ViewFactory.setTitle("Projetos");
            ViewFactory.setBreadcrumb([
                { title: "Home", icon: "fa fa-home", url: "/" },
                { title: "Projetos"},
            ]);
        }else{
            ViewFactory.setTitle("TAP - Termo de Abertura de Projeto");
            ViewFactory.setBreadcrumb([
                { title: "Home", icon: "fa fa-home", url: "/" },
                { title: "Projetos", icon: "fa fa-list", url: "/projetos" },
                { title: "TAP"}
            ]);
        }

        $ctrl.projetosList = ViewFactory.getSessionList("projetosList");
        if($ctrl.projetosList.length == 0){// Se não tiver na da sessão
            TapService.getProjetosList().then(function(response){
                $ctrl.projetosList = response;
                ViewFactory.setSession("projetosList", $ctrl.projetosList);
            });
        }

        MessagesService.getMessages().then(function(response){
            $ctrl.messages = response;
        });

        TapService.getMesReferenciaList().then(function(response){
            $ctrl.mesReferenciaList = response;
        });

        TapService.getClassificacaoList().then(function(response){
            $ctrl.classificacaoList = response;
        });

        
        $ctrl.projetosFields = [
            { label: "Descrição", id: "descricaoProjeto" },
            { label: "Data", id: "dataTAP", type:"date" },
            { label: "Mês Referência", id: "mesReferencia", type:"list" },
            { label: "Classificacao", id: "classificacao", type:"list" }
        ];

        $ctrl.acoesProjetos = [
            { label: "Editar", class: "btn-info", icon: "edit", callback: $ctrl.editar },
            { label: "Deletar", class: "btn-danger", icon: "trash", callback: $ctrl.remover },
            { label: "Anexar", dropdown: [
                { label: "Orçamento", icon: "plus", callback: function(item, index){ $ctrl.anexar(item, index, "/orcamento-projeto") } },
                { label: "Risco", icon: "plus", callback: function(item, index){ $ctrl.anexar(item, index, "/risco") } },
                { label: "Plano de Comunicação", icon: "plus", callback: function(item, index){ $ctrl.anexar(item, index, "/plano-comunicacao") } },
                { label: "Controle de Versão", icon: "plus", callback: function(item, index){ $ctrl.anexar(item, index, "/controle-versao") } },
                { label: "Encerrar Projeto", icon: "times", callback: function(item, index){ $ctrl.anexar(item, index, "/termo-encerramento-projeto") } },
            ]},
        ];
        
        $ctrl.camposFormalizacoes = [
            {id: "nome", label: "Nome", type: "text", required: true},
            {id: "data", label: "Data", type: "date", required: true},
            {id: "assinatura", label: "Confirmação Assinatura", type: "checkbox", required: true}
        ];

        function salvar(){
            if($scope.termoAberturaProjetoForm.$valid){
                if(index == -1){
                    $ctrl.projetosList.unshift($ctrl.model);
                }else{
                    $ctrl.projetosList[index] = $ctrl.model;
                }
                ViewFactory.setSession("projetosList", $ctrl.projetosList, "/projetos");
            }
        }

        function editar(item, index){
            $ctrl.model = item;
            ViewFactory.setSession("index", index);
            ViewFactory.setSession("projetoSelected", $ctrl.model, "/termo-abertura-projeto");
        }

        function anexar(item, index, rota){
            $ctrl.model = item;
            ViewFactory.setSession("index", index);
            ViewFactory.setSession("projetoSelected", $ctrl.model, rota);
        }

        function remover(item, index){
            $ctrl.projetosList.splice(index, 1);
            ViewFactory.setSession("projetosList", $ctrl.projetosList);
        }

        $ctrl.model = ViewFactory.getSessionObject("projetoSelected", new TapFactory());

    }]);

})();