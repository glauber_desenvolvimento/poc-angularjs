(function(){
    'use strict';

    angular.module("app", ["core"])
    .config(["$routeProvider", "$locationProvider", function($routeProvider, $locationProvider) {
        let routeMapping = window.routeMapping;
        for(var i=0; i<routeMapping.length; i++){
            let map = routeMapping[i];
            $routeProvider
            .when(map.url.replace("#", ''), {
                templateUrl: 'app/' + map.routing.templateUrl + "view.html",
                controller : map.routing.controller + " as $ctrl"
            });
        }
        $routeProvider.otherwise({redirectTo : "/"});
        if(window.location.origin == "http://localhost:3000"){
            $locationProvider.html5Mode(true);
            $('head').append('<base href="/" target="_blank">');
        }
        $locationProvider.hashPrefix('');
    }]);

})();